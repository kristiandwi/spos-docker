<!-- sidebar start -->
<div class="col-lg-4">
	<div class="sidebar">
		@include('includes.ads.desktop-sidebar-1')

		@include('includes.widget-popular-all')

		<style>
			.ads-image iframe {width: 300px !important;}
		</style>
		@include('includes.ads.desktop-sidebar-2')

		@include('includes.widget')

		@if( date('Y-m-d H:i:s') <= '2024-05-31 23:59:59')
		<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
		<script>
		window.googletag = window.googletag || {cmd: []};
		googletag.cmd.push(function() {
			googletag.defineSlot('/54058497/UKSW-MR-DESKTOP', [300, 250], 'div-gpt-ad-1634116572610-0').addService(googletag.pubads());
			googletag.pubads().enableSingleRequest();
			googletag.enableServices();
		});
		</script>
		<!-- /54058497/UKSW-MR-DESKTOP -->
		<div id='div-gpt-ad-1634116572610-0' style='min-width: 300px; min-height: 250px;'>
			<script>
			googletag.cmd.push(function() { googletag.display('div-gpt-ad-1634116572610-0'); });
			</script>
		</div><br>
		@endif

		<div class="sidebar-widget">
			<h2 class="block-title block-title-dark">
				<span class="title-angle-shap"> Urita </span>
			</h2>
			<div class="list-post-block">
                <ul class="list-post" id="urita">
                </ul><!-- List post end -->
            </div>
		</div>

		@include('includes.ads.desktop-sidebar-3')

		<div class="sidebar-widget">
			<h2 class="block-title block-title-dark">
				<span class="title-angle-shap"> Edukasi </span>
			</h2>
			<div class="list-post-block">
                <ul class="list-post" id="edukasi">
                </ul><!-- List post end -->
            </div>
		</div>

		@include('includes.ads.desktop-sidebar-4')

		<div class="sidebar-widget">
			<h2 class="block-title block-title-dark">
				<span class="title-angle-shap"> Mimbar Guru </span>
			</h2>
			<div class="list-post-block">
                <ul class="list-post" id="mimbarguru">
                </ul><!-- List post end -->
            </div>
		</div>
		<div class="sidebar-widget ads-widget">
			<div class="ads-image">
				<!-- PubMatic ad tag (Javascript) : solopos.com_Desktop_300x600 | TADEX_Web-PT_Aksara_Solopos- | 300 x 600 Filmstrip -->
				<script type="text/javascript">
					var pubId=157566;
					var siteId=841452;
					var kadId=4434918;
					var kadwidth=300;
					var kadheight=600;
					var kadschain="SUPPLYCHAIN_GOES_HERE";
					var kadUsPrivacy=""; <!-- Insert user CCPA consent string here for CCPA compliant inventory -->
					var kadtype=1;
					var kadGdpr="0"; <!-- set to 1 if inventory is GDPR compliant -->
					var kadGdprConsent=""; <!-- Insert user GDPR consent string here for GDPR compliant inventory -->
					var kadexpdir = '1,2,3,4,5';
					var kadbattr = '8,9,10,11,14';
					var kadifb = 'Dc';
					var kadpageurl= "%%PATTERN:url%%";
				</script>
				<script type="text/javascript" src="https://ads.pubmatic.com/AdServer/js/showad.js"></script>
			</div>
		</div>

	</div>
</div><!-- Sidebar Col end -->

@push('custom-scripts')
<script>
    function timeSince(date) {
        var seconds = Math.floor((new Date() - date) / 1000);

        var interval = seconds / 31536000;

        if (interval > 1) {
        return Math.floor(interval) + " tahun yang lalu";
        }
        interval = seconds / 2592000;
        if (interval > 1) {
        return Math.floor(interval) + " bulan yang lalu";
        }
        interval = seconds / 86400;
        if (interval > 1) {
        return Math.floor(interval) + " hari yang lalu";
        }
        interval = seconds / 3600;
        if (interval > 1) {
        return Math.floor(interval) + " jam yang lalu";
        }
        interval = seconds / 60;
        if (interval > 1) {
        return Math.floor(interval) + " menit yang lalu";
        }
        return Math.floor(seconds) + " detik yang lalu";
    }

    $(document).ready(function() {
        $.ajax({ //create an ajax request related post
        type: "GET",
        url: "https://api.solopos.com/api/breaking/posts?category=793214&per_page=4", //urita
        dataType: "JSON",
        success: function(data) {
            // console.log(data);
            var urita = $("#urita");

            $.each(data, function(i, item) {
                if(i < 4) {
                    urita.append("<li><div class=\"post-block-style media\"><div class=\"post-thumb\"><a href=\"https://sekolah.solopos.com/" + data[i]['slug'] + "-" + data[i]['id'] + "?utm_source=sidebar_desktop\" title=\"" + data[i]['title'] + "\"><img class=\"img-fluid\" src=\"" + data[i]['featured_image']['thumbnail'] +"\" alt=\"" + item['post_title'] + "\" style=\"object-fit: cover; object-position: center; height: 85px; width: 85px;\"></a></div><div class=\"post-content media-body\"><h2 class=\"post-title\"><a href=\"https://sekolah.solopos.com/" + data[i]['slug'] + "-" + data[i]['id'] + "?utm_source=sidebar_desktop\" title=\"" + data[i]['title'] + "\">" + data[i]['title'] + "</a></h2><div class=\"post-meta mb-7\"><span class=\"post-date\"><i class=\"fa fa-clock-o\"></i> " + timeSince(new Date(data[i]['date'])) + "</span></div></div></div></li>");
                }
            });
        }
        });
    });

    $(document).ready(function() {
        $.ajax({ //create an ajax request related post
        type: "GET",
        url: "https://api.solopos.com/api/breaking/posts?category=793216&per_page=4", //edukasi
        dataType: "JSON",
        success: function(data) {
            // console.log(data);
            var edukasi = $("#edukasi");

            $.each(data, function(i, item) {
                if(i < 4) {
                    edukasi.append("<li><div class=\"post-block-style media\"><div class=\"post-thumb\"><a href=\"https://sekolah.solopos.com/" + data[i]['slug'] + "-" + data[i]['id'] + "?utm_source=sidebar_desktop\" title=\"" + data[i]['title'] + "\"><img class=\"img-fluid\" src=\"" + data[i]['featured_image']['thumbnail'] +"\" alt=\"" + item['post_title'] + "\" style=\"object-fit: cover; object-position: center; height: 85px; width: 85px;\"></a></div><div class=\"post-content media-body\"><h2 class=\"post-title\"><a href=\"https://sekolah.solopos.com/" + data[i]['slug'] + "-" + data[i]['id'] + "?utm_source=sidebar_desktop\" title=\"" + data[i]['title'] + "\">" + data[i]['title'] + "</a></h2><div class=\"post-meta mb-7\"><span class=\"post-date\"><i class=\"fa fa-clock-o\"></i> " + timeSince(new Date(data[i]['date'])) + "</span></div></div></div></li>");
                }
            });
        }
        });
    });

    $(document).ready(function() {
        $.ajax({ //create an ajax request related post
        type: "GET",
        url: "https://api.solopos.com/api/breaking/posts?category=793218&per_page=4", //mimbar-guru
        dataType: "JSON",
        success: function(data) {
            // console.log(data);
            var mimbarguru = $("#mimbarguru");

            $.each(data, function(i, item) {
                if(i < 4) {
                    mimbarguru.append("<li><div class=\"post-block-style media\"><div class=\"post-thumb\"><a href=\"https://sekolah.solopos.com/" + data[i]['slug'] + "-" + data[i]['id'] + "?utm_source=sidebar_desktop\" title=\"" + data[i]['title'] + "\"><img class=\"img-fluid\" src=\"" + data[i]['featured_image']['thumbnail'] +"\" alt=\"" + item['post_title'] + "\" style=\"object-fit: cover; object-position: center; height: 85px; width: 85px;\"></a></div><div class=\"post-content media-body\"><h2 class=\"post-title\"><a href=\"https://sekolah.solopos.com/" + data[i]['slug'] + "-" + data[i]['id'] + "?utm_source=sidebar_desktop\" title=\"" + data[i]['title'] + "\">" + data[i]['title'] + "</a></h2><div class=\"post-meta mb-7\"><span class=\"post-date\"><i class=\"fa fa-clock-o\"></i> " + timeSince(new Date(data[i]['date'])) + "</span></div></div></div></li>");
                }
            });
        }
        });
    });
</script>
@endpush
