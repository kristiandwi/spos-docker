@php header('Access-Control-Allow-Origin: *');
@endphp
<!DOCTYPE html>
<html lang="id-ID">
    @include('regional.part.header')
    <body>
    <!-- Google Tag Manager (noscript) -->
    <!-- End Google Tag Manager (noscript) -->
    <!-- ads frame top -->
    @include('regional.part.header-section')
    @yield('content')
    @include('includes.footer')
    </body>
</html>
