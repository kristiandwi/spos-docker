@extends('layouts.app-sbbi')
@section('content')		
 			
			<div class="uk-cover-container uk-light uk-flex uk-flex-middle top-wrap-height">
				
				<!-- TOP CONTAINER 
				<div class="uk-container uk-flex-auto top-container uk-position-relative uk-margin-medium-top" data-uk-parallax="y: 0,50; easing:0; opacity:0.2">
					<div class="uk-width-1-2@s" data-uk-scrollspy="cls: uk-animation-slide-right-medium; target: > *; delay: 150">
						<h1 class="uk-margin-remove-top">
							<a href="https://www.solopos.com/sbbi/tentang-sbbi-award-2020" >
							SBBI - Innovation Awards
							</a>
						</h1>
					</div>
				</div>
				 /TOP CONTAINER -->
				<!-- TOP IMAGE -->
				
				{{-- <video autoplay loop uk-cover>
				    <source src="https://cms.solopos.com/elements/themes/desktop/sbbi/sbbi-2021.mp4" type='video/webm'>
				</video> --}}
				
				<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-srcset="https://cdn.solopos.com/sbbi/2022/bg-banner.jpg"
				data-sizes="12vw"
				data-src="https://picsum.photos/1200/900/?image=816" alt="" data-uk-cover data-uk-img data-uk-parallax="opacity: 1,0.1; easing:0"
				>
			</div>
			<div class="uk-position-bottom-center uk-position-medium uk-position-z-index uk-text-center">
				<a href="#sejarah" data-uk-scroll="duration: 500" data-uk-icon="icon: arrow-down; ratio: 2"></a>
			</div>
		</div>
		<!-- /TOP -->

		<!-- LOGOS -->
		<!--div id="partner" class="uk-container uk-container-small uk-section uk-section-small uk-section-muted" tabindex="-1" uk-slider>
			<div>
			    <ul class="uk-slider-items uk-child-width-1-4 uk-child-width-1-3@s uk-child-width-1-4@m logos-grid">
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/mitsubishi.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/mandiri.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/mandiri-syariah.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/bank-bri.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/kiat-motor.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/sun-motor.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/prodia.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/sharp.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/indofood.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/pku-sejati.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/djarum-super.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/djarum-76.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/djarum-mld.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/yamaha.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/candi-elektronik.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/best-western.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/telkom.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/solo-paragon.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/ums.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/superindo.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/astra.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/lorin-hotel.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/syariah-hotel.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/raya-seluler.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/ilufa.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/ella-skincare.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/nasmoco.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/daihatsu.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/kopi-luwak.jpg" data-uk-img alt="SBBI 2020">
					</li>
					<li>
						<img src="https://solopos.com/assets/ads/sbbi/tolak-angin.jpg" data-uk-img alt="SBBI 2020">
					</li>					
			    </ul>
				
		    </div>
		</div>
		 /LOGOS -->

		<section id="sejarah" class="uk-section uk-section-default">
			<div class="uk-container">
				<div class="uk-section uk-section-small uk-padding-remove-top">
					<ul class="uk-subnav uk-subnav-pill uk-flex uk-flex-center" data-uk-switcher="connect: .uk-switcher; animation: uk-animation-fade">
						<li><a class="uk-border-pill" href="#">Tentang SBBI</a></li>
						<li><a class="uk-border-pill" href="#">Tentang IMAB</a></li>
						<li><a class="uk-border-pill" href="#">Kilas Balik</a></li>
					</ul>
				</div>

				<ul class="uk-switcher uk-margin">
					<li>
						<div class="uk-grid uk-child-width-1-2@l uk-flex-middle" data-uk-grid data-uk-scrollspy="target: > div; cls: uk-animation-slide-left-medium">
							<div>
								<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="https://cdn.solopos.com/sbbi/2022/sbbi-thumb.jpg" alt="" data-uk-img>
								
								<!--img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="/sbbi/img/tentang-sbbi.png" alt="" data-uk-img-->
							</div>
							<div data-uk-scrollspy-class="uk-animation-slide-right-medium">
								<h6 class="uk-text-primary">SBBI AWARD 2022</h6>
								<h2 class="uk-margin-small-top">Solo Best Brand & Innovation Award 2022</h2>
								<p class="subtitle-text">
									Merek adalah kekayaan penting bagi sebuah perusahaan. Perlu strategi khusus untuk mengelola merek, terutama di era pandemi.
								</p>
								<div class="uk-grid uk-child-width-1-2@s" data-uk-grid>
									<div>
										<h4>Tujuan</h4>
										<p>Riset SBBI 2022 diharapkan dapat bermanfaat ... <a href="{{ url('/') }}/sbbi/tentang-sbbi-award-2022">Selengkapnya.</a></p>
									</div>
									<div>
										<h4>Kategori</h4>
										<p>SBBI Award 2022 dibagi menjadi 3 kategori... <a href="{{ url('/') }}/sbbi/tentang-sbbi-award-2022">Selengkapnya.</a></p>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="uk-grid uk-child-width-1-2@l uk-flex-middle" data-uk-grid data-uk-scrollspy="target: > div; cls: uk-animation-slide-left-medium">
							<div>
								<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="https://cdn.solopos.com/sbbi/2022/imab-thumb.jpg" alt="" data-uk-img>
							</div>
							<div data-uk-scrollspy-class="uk-animation-slide-right-medium">
								<h2 class="uk-margin-small-top">IMAB 2022</h2>
								<p class="subtitle-text">
									Di era serba digital, Solopos Media Group memberi sentuhan baru terhadap program penghargaan pada 2022. Bersamaan dengan pelaksanaan SBBI 2022, Solopos Media Group juga menggelar Indonesian Most Accelerate Brand atau IMAB 2022.
								</p>
								<div class="uk-grid uk-child-width-1-2@s" data-uk-grid>
									<div>
										<h4>Tujuan IMAB 2022</h4>
										<p>IMAB 2022 digelar dengan tujuan... <a href="{{ url('/') }}/sbbi/tentang-imab">Selengkapnya.</a></p>
									</div>
									<div>
										<h4>Kategori IMAB 2022</h4>
										<p>IMAB 2022 akan memberikan penghargaan kepada... <a href="{{ url('/') }}/sbbi/tentang-imab">Selengkapnya.</a></p>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="uk-grid uk-child-width-1-2@l uk-flex-middle" data-uk-grid data-uk-scrollspy="target: > div; cls: uk-animation-slide-left-medium">
							<div>
								<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="https://cms.solopos.com/elements/themes/desktop/sbbi/img/marketing-9.svg" alt="" data-uk-img>
							</div>
							<div data-uk-scrollspy-class="uk-animation-slide-right-medium">
								<h2 class="uk-margin-small-top">KILAS BALIK </h2>
								<p class="subtitle-text">
									SBBI lahir dari kepedulian Solopos Media Group terhadap belum adanya survei merek terbaik di kawasan Solo dan sekitarnya. Setiap tahun ada survei yang menilai merek terbaik secara Nasional, namun tidak ada yang menilai secara khusus pasar Soloraya.
								</p>
								<p class="subtitle-text">
									Padahal, kawasan Soloraya telah berkembang pesat dan memiliki karakter yang besar kemungkinan berbeda dengan Nasional.
								</p>

								<p class="subtitle-text">
									SBBI berawal pada 2000. Saat itu, survei merek terbaik ini masih menyandang nama Solo Customer Satisfaction Index (SCSI). SCSI berjalan selama enam tahun, 2000-2006. <a href="{{ url('/') }}/sbbi/kilas-balik">Selengkapnya.</a>
								</p>
							</div>
						</div>
					</li>
				</ul>
				
				
			</div>
		</section>

		
		<section id="angka" class="uk-section uk-section-secondary uk-section-large" style="background-color: #35c1f1">
			<div class="uk-container uk-container-xsmall uk-text-center uk-section uk-padding-remove-top">
				<h2 class="uk-margin-remove uk-h1">Video Pemenang SBBI 2020</h2>
			</div>	
					
			<div class="uk-container">
                <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slider>    
                    <ul class="uk-slider-items uk-child-width-1-3 uk-child-width-1-3@m uk-grid">
                        <li>
                            <div class="uk-panel">
                                <a href="#modal-media-youtube" uk-toggle>
                                    <img src="https://i1.ytimg.com/vi/pDh_-3ll6GY/mqdefault.jpg" alt="Thumbnail" style="width: 100%; height: auto;">
                                    <div class="uk-position-center uk-panel"><div uk-icon="icon: youtube; ratio: 3.5;"></div></div>
                                </a>
                            </div>
                        </li>                                                                   

                        <div id="modal-media-youtube" class="uk-flex-top" uk-modal>
                            <div class="uk-modal-dialog uk-width-auto uk-margin-auto-vertical">
                                <button class="uk-modal-close-outside" type="button" uk-close></button>
                                <iframe src="https://www.youtube-nocookie.com/embed/pDh_-3ll6GY?rel=0&vq=hd1080" width="1100" height="619" frameborder="0" uk-video uk-responsive></iframe>
                            </div>
                        </div>                          
                             
                        <li>
                            <div class="uk-panel">
                                <a href="#modal-media-youtube" uk-toggle>
                                    <img src="https://i1.ytimg.com/vi/9aLKJNsmh3w/mqdefault.jpg" alt="Thumbnail" style="width: 100%; height: auto;">
                                    <div class="uk-position-center uk-panel"><div uk-icon="icon: youtube; ratio: 3.5;"></div></div>
                                </a>
                            </div>
                        </li>                                                                   

                        <div id="modal-media-youtube" class="uk-flex-top" uk-modal>
                            <div class="uk-modal-dialog uk-width-auto uk-margin-auto-vertical">
                                <button class="uk-modal-close-outside" type="button" uk-close></button>
                                <iframe src="https://www.youtube-nocookie.com/embed/9aLKJNsmh3w?rel=0&vq=hd1080" width="1100" height="619" frameborder="0" uk-video uk-responsive></iframe>
                            </div>
                        </div>                          
                             
                        <li>
                            <div class="uk-panel">
                                <a href="#modal-media-youtube" uk-toggle>
                                    <img src="https://i1.ytimg.com/vi/d2KKxdpnSyw/mqdefault.jpg" alt="Thumbnail" style="width: 100%; height: auto;">
                                    <div class="uk-position-center uk-panel"><div uk-icon="icon: youtube; ratio: 3.5;"></div></div>
                                </a>
                            </div>
                        </li>                                                                   

                        <div id="modal-media-youtube" class="uk-flex-top" uk-modal>
                            <div class="uk-modal-dialog uk-width-auto uk-margin-auto-vertical">
                                <button class="uk-modal-close-outside" type="button" uk-close></button>
                                <iframe src="https://www.youtube-nocookie.com/embed/d2KKxdpnSyw?rel=0&vq=hd1080" width="1100" height="619" frameborder="0" uk-video uk-responsive></iframe>
                            </div>
                        </div>                          
                             
                        <li>
                            <div class="uk-panel">
                                <a href="#modal-media-youtube" uk-toggle>
                                    <img src="https://i1.ytimg.com/vi/UE1vEyMDP0E/mqdefault.jpg" alt="Thumbnail" style="width: 100%; height: auto;">
                                    <div class="uk-position-center uk-panel"><div uk-icon="icon: youtube; ratio: 3.5;"></div></div>
                                </a>
                            </div>
                        </li>                                                                   

                        <div id="modal-media-youtube" class="uk-flex-top" uk-modal>
                            <div class="uk-modal-dialog uk-width-auto uk-margin-auto-vertical">
                                <button class="uk-modal-close-outside" type="button" uk-close></button>
                                <iframe src="https://www.youtube-nocookie.com/embed/UE1vEyMDP0E?rel=0&vq=hd1080" width="1100" height="619" frameborder="0" uk-video uk-responsive></iframe>
                            </div>
                        </div>                          
                             
                        <li>
                            <div class="uk-panel">
                                <a href="#modal-media-youtube" uk-toggle>
                                    <img src="https://i1.ytimg.com/vi/4BiuqmkNyWI/mqdefault.jpg" alt="Thumbnail" style="width: 100%; height: auto;">
                                    <div class="uk-position-center uk-panel"><div uk-icon="icon: youtube; ratio: 3.5;"></div></div>
                                </a>
                            </div>
                        </li>                                                                   

                        <div id="modal-media-youtube" class="uk-flex-top" uk-modal>
                            <div class="uk-modal-dialog uk-width-auto uk-margin-auto-vertical">
                                <button class="uk-modal-close-outside" type="button" uk-close></button>
                                <iframe src="https://www.youtube-nocookie.com/embed/4BiuqmkNyWI?rel=0&vq=hd1080" width="1100" height="619" frameborder="0" uk-video uk-responsive></iframe>
                            </div>
                        </div>                          
                             
                        <li>
                            <div class="uk-panel">
                                <a href="#modal-media-youtube" uk-toggle>
                                    <img src="https://i1.ytimg.com/vi/RM_Szts6ZTE/mqdefault.jpg" alt="Thumbnail" style="width: 100%; height: auto;">
                                    <div class="uk-position-center uk-panel"><div uk-icon="icon: youtube; ratio: 3.5;"></div></div>
                                </a>
                            </div>
                        </li>                                                                   

                        <div id="modal-media-youtube" class="uk-flex-top" uk-modal>
                            <div class="uk-modal-dialog uk-width-auto uk-margin-auto-vertical">
                                <button class="uk-modal-close-outside" type="button" uk-close></button>
                                <iframe src="https://www.youtube-nocookie.com/embed/RM_Szts6ZTE?rel=0&vq=hd1080" width="1100" height="619" frameborder="0" uk-video uk-responsive></iframe>
                            </div>
                        </div>                          
                             
                        <li>
                            <div class="uk-panel">
                                <a href="#modal-media-youtube" uk-toggle>
                                    <img src="https://i1.ytimg.com/vi/RM_Szts6ZTE/mqdefault.jpg" alt="Thumbnail" style="width: 100%; height: auto;">
                                    <div class="uk-position-center uk-panel"><div uk-icon="icon: youtube; ratio: 3.5;"></div></div>
                                </a>
                            </div>
                        </li>                                                                   

                        <div id="modal-media-youtube" class="uk-flex-top" uk-modal>
                            <div class="uk-modal-dialog uk-width-auto uk-margin-auto-vertical">
                                <button class="uk-modal-close-outside" type="button" uk-close></button>
                                <iframe src="https://www.youtube-nocookie.com/embed/RM_Szts6ZTE?rel=0&vq=hd1080" width="1100" height="619" frameborder="0" uk-video uk-responsive></iframe>
                            </div>
                        </div>                          
                             
                        <li>
                            <div class="uk-panel">
                                <a href="#modal-media-youtube" uk-toggle>
                                    <img src="https://i1.ytimg.com/vi/RM_Szts6ZTE/mqdefault.jpg" alt="Thumbnail" style="width: 100%; height: auto;">
                                    <div class="uk-position-center uk-panel"><div uk-icon="icon: youtube; ratio: 3.5;"></div></div>
                                </a>
                            </div>
                        </li>                                                                   

                        <div id="modal-media-youtube" class="uk-flex-top" uk-modal>
                            <div class="uk-modal-dialog uk-width-auto uk-margin-auto-vertical">
                                <button class="uk-modal-close-outside" type="button" uk-close></button>
                                <iframe src="https://www.youtube-nocookie.com/embed/RM_Szts6ZTE?rel=0&vq=hd1080" width="1100" height="619" frameborder="0" uk-video uk-responsive></iframe>
                            </div>
                        </div>                          
                             
                        <li>
                            <div class="uk-panel">
                                <a href="#modal-media-youtube" uk-toggle>
                                    <img src="https://i1.ytimg.com/vi/G8fvd2yLQH8/mqdefault.jpg" alt="Thumbnail" style="width: 100%; height: auto;">
                                    <div class="uk-position-center uk-panel"><div uk-icon="icon: youtube; ratio: 3.5;"></div></div>
                                </a>
                            </div>
                        </li>                                                                   

                        <div id="modal-media-youtube" class="uk-flex-top" uk-modal>
                            <div class="uk-modal-dialog uk-width-auto uk-margin-auto-vertical">
                                <button class="uk-modal-close-outside" type="button" uk-close></button>
                                <iframe src="https://www.youtube-nocookie.com/embed/G8fvd2yLQH8?rel=0&vq=hd1080" width="1100" height="619" frameborder="0" uk-video uk-responsive></iframe>
                            </div>
                        </div>                          
                             
                        <li>
                            <div class="uk-panel">
                                <a href="#modal-media-youtube" uk-toggle>
                                    <img src="https://i1.ytimg.com/vi/ldAIzKMreQU/mqdefault.jpg" alt="Thumbnail" style="width: 100%; height: auto;">
                                    <div class="uk-position-center uk-panel"><div uk-icon="icon: youtube; ratio: 3.5;"></div></div>
                                </a>
                            </div>
                        </li>                                                                   

                        <div id="modal-media-youtube" class="uk-flex-top" uk-modal>
                            <div class="uk-modal-dialog uk-width-auto uk-margin-auto-vertical">
                                <button class="uk-modal-close-outside" type="button" uk-close></button>
                                <iframe src="https://www.youtube-nocookie.com/embed/ldAIzKMreQU?rel=0&vq=hd1080" width="1100" height="619" frameborder="0" uk-video uk-responsive></iframe>
                            </div>
                        </div>                          
                             
                        <li>
                            <div class="uk-panel">
                                <a href="#modal-media-youtube" uk-toggle>
                                    <img src="https://i1.ytimg.com/vi/wgWGBRrez70/mqdefault.jpg" alt="Thumbnail" style="width: 100%; height: auto;">
                                    <div class="uk-position-center uk-panel"><div uk-icon="icon: youtube; ratio: 3.5;"></div></div>
                                </a>
                            </div>
                        </li>                                                                   

                        <div id="modal-media-youtube" class="uk-flex-top" uk-modal>
                            <div class="uk-modal-dialog uk-width-auto uk-margin-auto-vertical">
                                <button class="uk-modal-close-outside" type="button" uk-close></button>
                                <iframe src="https://www.youtube-nocookie.com/embed/wgWGBRrez70?rel=0&vq=hd1080" width="1100" height="619" frameborder="0" uk-video uk-responsive></iframe>
                            </div>
                        </div>                          
                             
                        <li>
                            <div class="uk-panel">
                                <a href="#modal-media-youtube" uk-toggle>
                                    <img src="https://i1.ytimg.com/vi/60A3BhVe28A/mqdefault.jpg" alt="Thumbnail" style="width: 100%; height: auto;">
                                    <div class="uk-position-center uk-panel"><div uk-icon="icon: youtube; ratio: 3.5;"></div></div>
                                </a>
                            </div>
                        </li>                                                                   

                        <div id="modal-media-youtube" class="uk-flex-top" uk-modal>
                            <div class="uk-modal-dialog uk-width-auto uk-margin-auto-vertical">
                                <button class="uk-modal-close-outside" type="button" uk-close></button>
                                <iframe src="https://www.youtube-nocookie.com/embed/60A3BhVe28A?rel=0&vq=hd1080" width="1100" height="619" frameborder="0" uk-video uk-responsive></iframe>
                            </div>
                        </div>                          
                             
                        <li>
                            <div class="uk-panel">
                                <a href="#modal-media-youtube" uk-toggle>
                                    <img src="https://i1.ytimg.com/vi/4QcTT8ItEsU/mqdefault.jpg" alt="Thumbnail" style="width: 100%; height: auto;">
                                    <div class="uk-position-center uk-panel"><div uk-icon="icon: youtube; ratio: 3.5;"></div></div>
                                </a>
                            </div>
                        </li>                                                                   

                        <div id="modal-media-youtube" class="uk-flex-top" uk-modal>
                            <div class="uk-modal-dialog uk-width-auto uk-margin-auto-vertical">
                                <button class="uk-modal-close-outside" type="button" uk-close></button>
                                <iframe src="https://www.youtube-nocookie.com/embed/4QcTT8ItEsU?rel=0&vq=hd1080" width="1100" height="619" frameborder="0" uk-video uk-responsive></iframe>
                            </div>
                        </div>                          
                             
                        <li>
                            <div class="uk-panel">
                                <a href="#modal-media-youtube" uk-toggle>
                                    <img src="https://i1.ytimg.com/vi/gxYm_-O83t4/mqdefault.jpg" alt="Thumbnail" style="width: 100%; height: auto;">
                                    <div class="uk-position-center uk-panel"><div uk-icon="icon: youtube; ratio: 3.5;"></div></div>
                                </a>
                            </div>
                        </li>                                                                   

                        <div id="modal-media-youtube" class="uk-flex-top" uk-modal>
                            <div class="uk-modal-dialog uk-width-auto uk-margin-auto-vertical">
                                <button class="uk-modal-close-outside" type="button" uk-close></button>
                                <iframe src="https://www.youtube-nocookie.com/embed/gxYm_-O83t4?rel=0&vq=hd1080" width="1100" height="619" frameborder="0" uk-video uk-responsive></iframe>
                            </div>
                        </div>                          
                             
                        <li>
                            <div class="uk-panel">
                                <a href="#modal-media-youtube" uk-toggle>
                                    <img src="https://i1.ytimg.com/vi/uw7Mlid7HUk/mqdefault.jpg" alt="Thumbnail" style="width: 100%; height: auto;">
                                    <div class="uk-position-center uk-panel"><div uk-icon="icon: youtube; ratio: 3.5;"></div></div>
                                </a>
                            </div>
                        </li>                                                                   

                        <div id="modal-media-youtube" class="uk-flex-top" uk-modal>
                            <div class="uk-modal-dialog uk-width-auto uk-margin-auto-vertical">
                                <button class="uk-modal-close-outside" type="button" uk-close></button>
                                <iframe src="https://www.youtube-nocookie.com/embed/uw7Mlid7HUk?rel=0&vq=hd1080" width="1100" height="619" frameborder="0" uk-video uk-responsive></iframe>
                            </div>
                        </div> 
                    </ul>
                    <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                    <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a> 
                </div>                         
			</div>
		</section>
		

		<section id="news" class="uk-section uk-section-default">
			
			<div class="uk-container uk-container-xsmall uk-text-center uk-section uk-padding-remove-top">
				<h2 class="uk-margin-remove uk-h1">Informasi Terbaru SBBI 2022</h2>
			</div>
			<div class="uk-container">

				<div class="uk-child-width-1-3@m" uk-grid>
                    @php
	                    $loop_no = 1;
	                    @endphp
                        @foreach ($sbbi as $posts)
						@php           
						$thumb = $posts['featured_image']['thumbnail'] ?? 'https://www.solopos.com/images/no-thumb.jpg'; 
            			$medium = $posts['featured_image']['medium'] ?? 'https://www.solopos.com/images/no-medium.jpg';
						$title = html_entity_decode($posts['title']);
					@endphp
                    @if($loop_no<=15)  
                    <div class="uk-card uk-card-default">
                        <div class="uk-card-badge uk-label">{{ Carbon\Carbon::parse($posts['date'])->translatedFormat('j F Y') }}</div>
                        <div class="uk-card-media-top">
                            <a href="{{ url("/{$posts['slug']}-{$posts['id']}") }}" title="{{ $title }}">
                                <img src="{{ $medium }}" alt="{{ $title }}">
                            </a>
                        </div>
                        <div class="uk-card-body">
                            <a href="{{ url("/{$posts['slug']}-{$posts['id']}") }}" title="{{ $title }}" target="_blank">
                                <h3 class="uk-card-title">{{ $title }}</h3>
                            </a>
                        </div>
                    </div>
                    @endif
                    @php $loop_no++; @endphp
					@endforeach
                     
                    {{--
                    <div class="uk-card uk-card-default">
                        <div class="uk-card-badge uk-label">5 Agustus 2021</div>
                        <div class="uk-card-media-top">
                            <a href="https://www.solopos.com/best-western-premier-solo-baru-kembali-raih-penghargaan-sbbi-1143793" title="Best Western Premier Solo Baru Kembali Raih Penghargaan SBBI">
                                <img src="https://images.solopos.com/2021/08/sertifikat.jpg" alt="Best Western Premier Solo Baru Kembali Raih Penghargaan SBBI">
                            </a>
                        </div>
                        <div class="uk-card-body">
                            <a href="https://www.solopos.com/best-western-premier-solo-baru-kembali-raih-penghargaan-sbbi-1143793" title="Best Western Premier Solo Baru Kembali Raih Penghargaan SBBI" target="_blank">
                                <h3 class="uk-card-title">Best Western Premier Solo Baru Kembali Raih Penghargaan SBBI</h3>
                            </a>
                        </div>
                    </div>

                    <div class="uk-card uk-card-default">
                        <div class="uk-card-badge uk-label">4 Agustus 2021</div>
                        <div class="uk-card-media-top">
                            <a href="https://www.solopos.com/siapa-kepala-daerah-dengan-performa-digital-terbaik-simak-malam-ini-pengumuman-sda-2021-1143549" title="Siapa Kepala Daerah dengan Performa Digital Terbaik? Simak Malam Ini Pengumuman SDA 2021">
                                <img src="https://images.solopos.com/2021/08/0408SDA-2021.jpeg" alt="Siapa Kepala Daerah dengan Performa Digital Terbaik? Simak Malam Ini Pengumuman SDA 2021">
                            </a>
                        </div>
                        <div class="uk-card-body">
                            <a href="https://www.solopos.com/siapa-kepala-daerah-dengan-performa-digital-terbaik-simak-malam-ini-pengumuman-sda-2021-1143549" title="Siapa Kepala Daerah dengan Performa Digital Terbaik? Simak Malam Ini Pengumuman SDA 2021" target="_blank">
                                <h3 class="uk-card-title">Siapa Kepala Daerah dengan Performa Digital Terbaik? Simak Malam Ini Pengumuman SDA 2021</h3>
                            </a>
                        </div>
                    </div>
                                             
                    <div class="uk-card uk-card-default">
                        <div class="uk-card-badge uk-label">3 Agustus 2021</div>
                        <div class="uk-card-media-top">
                            <a href="https://www.solopos.com/tak-hanya-harga-kualitas-jadi-kunci-semen-dynamix-diterima-konsumen-1143311" title="Tak Hanya Harga, Kualitas Jadi Kunci Semen Dynamix Diterima Konsumen">
                                <img src="https://images.solopos.com/2021/08/dynamix.jpg" alt="Tak Hanya Harga, Kualitas Jadi Kunci Semen Dynamix Diterima Konsumen">
                            </a>
                        </div>
                        <div class="uk-card-body">
                            <a href="https://www.solopos.com/tak-hanya-harga-kualitas-jadi-kunci-semen-dynamix-diterima-konsumen-1143311" title="Tak Hanya Harga, Kualitas Jadi Kunci Semen Dynamix Diterima Konsumen" target="_blank">
                                <h3 class="uk-card-title">Tak Hanya Harga, Kualitas Jadi Kunci Semen Dynamix Diterima Konsumen</h3>
                            </a>
                        </div>
                    </div>
                                             
                    <div class="uk-card uk-card-default">
                        <div class="uk-card-badge uk-label">3 Agustus 2021</div>
                        <div class="uk-card-media-top">
                            <a href="https://www.solopos.com/ini-rahasia-sharp-terus-survive-di-masa-pandemi-covid-19-1143304" title="Ini Rahasia Sharp Terus Survive di Masa Pandemi Covid-19">
                                <img src="https://images.solopos.com/2021/08/sharp-2.jpg" alt="Ini Rahasia Sharp Terus Survive di Masa Pandemi Covid-19">
                            </a>
                        </div>
                        <div class="uk-card-body">
                            <a href="https://www.solopos.com/ini-rahasia-sharp-terus-survive-di-masa-pandemi-covid-19-1143304" title="Ini Rahasia Sharp Terus Survive di Masa Pandemi Covid-19" target="_blank">
                                <h3 class="uk-card-title">Ini Rahasia Sharp Terus Survive di Masa Pandemi Covid-19</h3>
                            </a>
                        </div>
                    </div>
                                             
                    <div class="uk-card uk-card-default">
                        <div class="uk-card-badge uk-label">3 Agustus 2021</div>
                        <div class="uk-card-media-top">
                            <a href="https://www.solopos.com/selamat-merek-merek-ini-sukses-juara-sbbi-bertahun-tahun-1143290" title="Selamat! Merek-Merek Ini Sukses Juara SBBI Bertahun-Tahun">
                                <img src="https://images.solopos.com/2021/08/0308SBBI-sesi-II.jpeg" alt="Selamat! Merek-Merek Ini Sukses Juara SBBI Bertahun-Tahun">
                            </a>
                        </div>
                        <div class="uk-card-body">
                            <a href="https://www.solopos.com/selamat-merek-merek-ini-sukses-juara-sbbi-bertahun-tahun-1143290" title="Selamat! Merek-Merek Ini Sukses Juara SBBI Bertahun-Tahun" target="_blank">
                                <h3 class="uk-card-title">Selamat! Merek-Merek Ini Sukses Juara SBBI Bertahun-Tahun</h3>
                            </a>
                        </div>
                    </div>
                                             
                    <div class="uk-card uk-card-default">
                        <div class="uk-card-badge uk-label">3 Agustus 2021</div>
                        <div class="uk-card-media-top">
                            <a href="https://www.solopos.com/ini-merek-terbaik-mal-bank-hingga-toko-hp-di-sbbi-2021-pilihanmu-ada-1143273" title="Ini Merek Terbaik Mal, Bank, hingga Toko HP di SBBI 2021, Pilihanmu Ada?">
                                <img src="https://images.solopos.com/2021/08/0308SBBI-sesi-II.jpeg" alt="Ini Merek Terbaik Mal, Bank, hingga Toko HP di SBBI 2021, Pilihanmu Ada?">
                            </a>
                        </div>
                        <div class="uk-card-body">
                            <a href="https://www.solopos.com/ini-merek-terbaik-mal-bank-hingga-toko-hp-di-sbbi-2021-pilihanmu-ada-1143273" title="Ini Merek Terbaik Mal, Bank, hingga Toko HP di SBBI 2021, Pilihanmu Ada?" target="_blank">
                                <h3 class="uk-card-title">Ini Merek Terbaik Mal, Bank, hingga Toko HP di SBBI 2021, Pilihanmu Ada?</h3>
                            </a>
                        </div>
                    </div>
                                             
                    <div class="uk-card uk-card-default">
                        <div class="uk-card-badge uk-label">3 Agustus 2021</div>
                        <div class="uk-card-media-top">
                            <a href="https://www.solopos.com/keren-sharp-raih-penghargaan-lemari-es-terbaik-di-sbbi-award-2021-1143269" title="Keren! Sharp Raih Penghargaan Lemari Es Terbaik di SBBI Award 2021">
                                <img src="https://images.solopos.com/2021/08/sharp.jpg" alt="Keren! Sharp Raih Penghargaan Lemari Es Terbaik di SBBI Award 2021">
                            </a>
                        </div>
                        <div class="uk-card-body">
                            <a href="https://www.solopos.com/keren-sharp-raih-penghargaan-lemari-es-terbaik-di-sbbi-award-2021-1143269" title="Keren! Sharp Raih Penghargaan Lemari Es Terbaik di SBBI Award 2021" target="_blank">
                                <h3 class="uk-card-title">Keren! Sharp Raih Penghargaan Lemari Es Terbaik di SBBI Award 2021</h3>
                            </a>
                        </div>
                    </div>
                                             
                    <div class="uk-card uk-card-default">
                        <div class="uk-card-badge uk-label">3 Agustus 2021</div>
                        <div class="uk-card-media-top">
                            <a href="https://www.solopos.com/mantap-yamaha-nmax-jadi-pemenang-di-sbbi-award-2021-kategori-motor-matic-sport-1143256" title="Mantap! Yamaha NMAX Jadi Pemenang di SBBI Award 2021 Kategori Motor Matic Sport">
                                <img src="https://images.solopos.com/2021/08/yamaha-nmax.jpg" alt="Mantap! Yamaha NMAX Jadi Pemenang di SBBI Award 2021 Kategori Motor Matic Sport">
                            </a>
                        </div>
                        <div class="uk-card-body">
                            <a href="https://www.solopos.com/mantap-yamaha-nmax-jadi-pemenang-di-sbbi-award-2021-kategori-motor-matic-sport-1143256" title="Mantap! Yamaha NMAX Jadi Pemenang di SBBI Award 2021 Kategori Motor Matic Sport" target="_blank">
                                <h3 class="uk-card-title">Mantap! Yamaha NMAX Jadi Pemenang di SBBI Award 2021 Kategori Motor Matic Sport</h3>
                            </a>
                        </div>
                    </div>
                                             
                    <div class="uk-card uk-card-default">
                        <div class="uk-card-badge uk-label">3 Agustus 2021</div>
                        <div class="uk-card-media-top">
                            <a href="https://www.solopos.com/buka-bukaan-begini-cerita-dibalik-riset-merek-terpercaya-sbbi-2021-1143268" title="Buka-Bukaan, Begini Cerita Dibalik Riset Merek Terpercaya SBBI 2021">
                                <img src="https://images.solopos.com/2021/04/2604SBBI-2021.jpeg" alt="Buka-Bukaan, Begini Cerita Dibalik Riset Merek Terpercaya SBBI 2021">
                            </a>
                        </div>
                        <div class="uk-card-body">
                            <a href="https://www.solopos.com/buka-bukaan-begini-cerita-dibalik-riset-merek-terpercaya-sbbi-2021-1143268" title="Buka-Bukaan, Begini Cerita Dibalik Riset Merek Terpercaya SBBI 2021" target="_blank">
                                <h3 class="uk-card-title">Buka-Bukaan, Begini Cerita Dibalik Riset Merek Terpercaya SBBI 2021</h3>
                            </a>
                        </div>
                    </div>
                                             
                    <div class="uk-card uk-card-default">
                        <div class="uk-card-badge uk-label">3 Agustus 2021</div>
                        <div class="uk-card-media-top">
                            <a href="https://www.solopos.com/meski-virtual-anugerah-sbbi-award-2021-digelar-mewah-lur-1143258" title="Meski Virtual, Anugerah SBBI Award 2021 Digelar Mewah Lur!!">
                                <img src="https://images.solopos.com/2021/08/sbbi-2021-acara.jpg" alt="Meski Virtual, Anugerah SBBI Award 2021 Digelar Mewah Lur!!">
                            </a>
                        </div>
                        <div class="uk-card-body">
                            <a href="https://www.solopos.com/meski-virtual-anugerah-sbbi-award-2021-digelar-mewah-lur-1143258" title="Meski Virtual, Anugerah SBBI Award 2021 Digelar Mewah Lur!!" target="_blank">
                                <h3 class="uk-card-title">Meski Virtual, Anugerah SBBI Award 2021 Digelar Mewah Lur!!</h3>
                            </a>
                        </div>
                    </div>
                                             
                    <div class="uk-card uk-card-default">
                        <div class="uk-card-badge uk-label">2 Agustus 2021</div>
                        <div class="uk-card-media-top">
                            <a href="https://www.solopos.com/banjir-hadiah-jangan-lewatkan-anugerah-sbbi-2021-yang-digelar-besok-1143037" title="Banjir Hadiah, Jangan Lewatkan Anugerah SBBI 2021 yang Digelar Besok!">
                                <img src="https://images.solopos.com/2021/08/SBBI-2021-rs.jpg" alt="Banjir Hadiah, Jangan Lewatkan Anugerah SBBI 2021 yang Digelar Besok!">
                            </a>
                        </div>
                        <div class="uk-card-body">
                            <a href="https://www.solopos.com/banjir-hadiah-jangan-lewatkan-anugerah-sbbi-2021-yang-digelar-besok-1143037" title="Banjir Hadiah, Jangan Lewatkan Anugerah SBBI 2021 yang Digelar Besok!" target="_blank">
                                <h3 class="uk-card-title">Banjir Hadiah, Jangan Lewatkan Anugerah SBBI 2021 yang Digelar Besok!</h3>
                            </a>
                        </div>
                    </div>
                                             
                    <div class="uk-card uk-card-default">
                        <div class="uk-card-badge uk-label">2 Agustus 2021</div>
                        <div class="uk-card-media-top">
                            <a href="https://www.solopos.com/tak-perlu-diragukan-riset-sbbi-solopos-terjamin-1143111" title="Tak Perlu Diragukan, Riset SBBI Solopos Terjamin">
                                <img src="https://images.solopos.com/2021/05/thumbnail-microsite-SBBI-01.png" alt="Tak Perlu Diragukan, Riset SBBI Solopos Terjamin">
                            </a>
                        </div>
                        <div class="uk-card-body">
                            <a href="https://www.solopos.com/tak-perlu-diragukan-riset-sbbi-solopos-terjamin-1143111" title="Tak Perlu Diragukan, Riset SBBI Solopos Terjamin" target="_blank">
                                <h3 class="uk-card-title">Tak Perlu Diragukan, Riset SBBI Solopos Terjamin</h3>
                            </a>
                        </div>
                    </div>--}}
				</div>

			</div>
		</section>
		<!--
		<section id="testimoni" class="uk-cover-container overlay-wrap">
			<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-srcset="/sbbi/img/bg.jpg"
			data-sizes="100vw"
			data-src="/sbbi/img/bg.jpg" alt="" data-uk-cover data-uk-img
			>
			<div class="uk-container uk-position-z-index uk-position-relative uk-section uk-section-xlarge uk-light">
				<div class="uk-grid uk-flex-right">
					
					<div class="uk-width-2-5@m" data-uk-parallax="y: 50,-50; easing: 0; media:@l">
						<h6>TESTIMONIALS</h6>
						<div class="uk-position-relative uk-visible-toggle uk-light" data-uk-slider="easing: cubic-bezier(.16,.75,.47,1)">
							<ul class="uk-slider-items uk-child-width-1-1">
								<li>
									<div data-uk-slider-parallax="opacity: 0.2,1,0.2">
										<h2 class="uk-margin-small-top">"Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur"</h2>
										<p class="uk-text-meta">Lorena Smith, founder of Some Cool Startup</p>
									</div>
								</li>
								<li>
									<div data-uk-slider-parallax="opacity: 0.2,1,0.2">
										<h2 class="uk-margin-small-top">"Aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur"</h2>
										<p class="uk-text-meta">Lorena Smith, founder of Some Cool Startup</p>
									</div>
								</li>
								<li>
									<div data-uk-slider-parallax="opacity: 0.2,1,0.2">
										<h2 class="uk-margin-small-top">"Irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur"</h2>
										<p class="uk-text-meta">Lorena Smith, founder of Some Cool Startup</p>
									</div>
								</li>
							</ul>
							<ul class="uk-slider-nav uk-dotnav uk-margin-top"><li></li></ul>
							
						</div>
					</div>
					
				</div>
			</div>
		</section> -->
@endsection        