@extends('layouts.app')
@section('content')
	<section class="main-content category-layout-1 pt-0">
		<div class="container">
			<div class="post-header-area" align="center">
				<h2 class="post-title title-lg">Kontak Kami</h2>
				<img class="w-100 single-blog-image" src="https://images.solopos.com/2020/07/solopos-1.jpg" alt="Solopos Digital Media">
			</div>	
			
			<div class="gap-30"></div>
			
			<div class="row ts-gutter-30">
				<div class="container">
					<p>Griya SOLOPOS<br />
					  Jl. Adisucipto 190 Solo Lt. 2<br />
					  Telp. (0271) 724811<br />
					  Fax. (0271) 724833 (REDAKSI)<br />
					  WA. +62 817-724-811</p>
		   
				  </div>																
			</div><!-- row end -->
			
		</div><!-- container end -->
	</section><!-- category-layout end -->

@endsection