@extends('layouts.app')
@section('content')
	<!-- breadcrumb -->
	<div class="breadcrumb-section">
		<div class="container pr-0 pl-0">
			<div class="row">
				<div class="col-lg-10 mx-auto"">
					@if(Cookie::get('is_login') == 'true')
					<div class="author-box d-flex alert alert-primary alert-dismissable" style="padding-left:30px; padding-right:30px;margin-top:0;">
						<div class="author-info" style="margin-left:0;margin-right:30px;">
							<h6 style="margin-top:0;font-size:14px;font-weight:400">
								<a href="https://id.solopos.com/profile">{{ Helper::greetings() }} <strong>{{ Cookie::get('is_name') }}</strong>, Bagaimana Kabarnya?</a>
							</h6>
							<p style="font-size:13px; line-height:19px;padding-top:10px;">Di ekosistem Solopos Media Group kami percaya member harus jadi prioritas utama. Hubungi Customer Service kami dengan mengklik nomer ini <strong><a href="https://api.whatsapp.com/send/?phone=6289510318382&text&type=phone_number&app_absent=0" title="Kontak Admin">089510318382 (WhatsApp)</a></strong> apabila Anda memiliki pertanyaan, saran, kritik, atau mengalami kendala terkait layanan Espos Plus. Kami ada untuk memberikan yang terbaik bagi Anda.</p>
						</div>
						<div class="align-items-center">
							<img class="align-bottom" src="https://id.solopos.com/images/logo.png" width="100" alt="Solopos.id">
							<div class="author-info" style="margin-left:0;">
								<div style="margin-top:15px; text-align:center;">
									<button class="btn btn-success btn-sm" style="padding:.25rem 1rem;"><a href="https://id.solopos.com" style="color:#fff;">Kelola Akun</a></button>
								</div>
							</div>
						</div>
					</div>	
					@endif									
					<ol class="breadcrumb" style="padding-top: 0;">
						<li>
							<a href="https://www.solopos.com/"><i class="fa fa-home"></i></a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li><a href="{{ url('/plus') }}">Espos Plus</a></li>
						<li><i class="fa fa-angle-right"></i>{{ html_entity_decode($content['title']) }}</li>
					</ol>					
				</div>
			</div><!-- row end -->
		</div><!-- container end -->
	</div>
	<!-- breadcrumb end -->
	<!-- mfunc setPostViews(get_the_ID()); --><!--/mfunc-->
	<section class="main-content pt-0">
		<div class="container pl-0 pr-0">
			<div class="row ts-gutter-30">
				<div class="col-lg-10 mx-auto">
					<div class="single-post">
						<div class="post-header-area">
							<h1 class="post-title title-lg">{{ $header['title'] }}</h1>
							<p>{{ $header['ringkasan'] }}</p>
							<ul class="post-meta">
								<li>
									<a class="post-cat {{ $header['category_parent'] }}" href="{{ url('/') }}/{{ $content['category_parent'] }}">{{ $header['category_parent'] }}</a>
								</li>
								<li><i class="fa fa-clock-o"></i>{{ Helper::indo_datetime($content['date']) }} WIB</li>
								<li>
									@if($header['author'] != $header['editor'] )
										Penulis:
										@foreach ($author_slug as $author)
										<a href="@if (ucwords(str_replace('_', ' ', $author)) ==  $header['editor'] ) {{ url('/')}}/author/{{ $header['editor_url'] }} @else {{ url('/') }}/penulis/{{$author}} @endif" class="penulis">{{ ucwords(str_replace('_', ' ', $author)) }}</a> <span style="margin-right:3px; margin-left:3px;padding-right:0;">|</span>
										@endforeach
									@endif
									Editor:  <a href="@if(!empty($header['editor_url'])){{ url('/')}}/author/{{ $header['editor_url'] }} @else https://www.solopos.com/arsip @endif" class="penulis" target="_blank">{{ $header['editor'] }}</a>
								</li>
								{{-- <li><a href="#"><i class="fa fa-eye"></i><!-- reading time --></a></li> --}}
								<li class="social-share">
									<i class="shareicon fa fa-share"></i>
									<ul class="social-list">
										<li><a data-social="facebook" class="facebook" href="https://www.facebook.com/sharer/sharer.php?u={{ $header['link'] }}" target="_blank" rel="noopener" title="social"><i class="fa fa-facebook"></i></a></li>
										<li><a data-social="twitter" class="twitter" href="https://twitter.com/home?status={{ $header['link'] }}" target="_blank" rel="noopener" title="social"><i class="fa fa-twitter"></i></a></li>
										<li><a data-social="whatsapp" class="whatsapp" href="https://web.whatsapp.com/send?text=*{{ urlencode($header['title']) }}* | {{ $header['ringkasan'] }} |  _selengkapnya baca di sini_ {{ $header['link'] }}" target="_blank" rel="noopener" title="social"><i class="fa fa-whatsapp"></i></a></li>
									</ul>
								</li>
							</ul>
						</div><!-- post-header-area end -->
						<div class="post-content-area">
							<div class="post-media mb-20" style="max-height:450px;">
								<a href="{{ $content['image']}}" class="gallery-popup cboxElement">
									<img src="{{ $content['image']}}" alt="@if(!empty($content['caption'])) {{ htmlentities($content['caption']) }} @endif" class="img-fluid" style="object-fit: cover; object-position: center; width: 811px; height: 450px;">
								</a>
								<span>
                                    @if(!empty($content['caption']))
                                        <p>SOLOPOS.COM - {{ htmlentities($content['caption']) }}</p>
                                    @else
                                        <p>SOLOPOS.COM - Panduan Informasi dan Inspirasi</p>
                                    @endif
                                </span>
							</div>
							@php
							$konten = Helper::konten(htmlspecialchars_decode($data['content']['content'])) ;
							$contents = explode('</p>', $konten);
							$total_p = count(array_filter($contents));
							$intro  = array_slice($contents, 0, 3);
							@endphp

							@if(Cookie::get('is_login') == 'true')
							<div id="konten-premium" class="premium">
								@if(Cookie::get('is_membership') == 'free')
								<div id="promosi">
									{!! implode('</p>', $intro) !!}
									<div style="background-image: linear-gradient(
									180deg,hsla(0,0%,100%,0),#fff);height:5rem;width:100%;position:relative;margin-top:-90px;"></div>
									<div class="subscribe mx-auto">
										<div class="row justify-content-center">
											<div class="col-md-12">
												<div class="header-box">
													<img class="logo" src="{{ url('images/plus.png') }}" alt="logo">
													<div class="login">
														<a href="https://id.solopos.com/subscribe?ref={{ request()->fullUrl() }}" title="Langganan Espos Plus">Aktifkan Paket <i class="fa fa-arrow-circle-o-right"></i></a>
													</div>
												</div>
												<div class="top-box">
													<div class="lead">Lanjutkan Membaca...</div>
													<div class="sublead">
														Hi {{ Cookie::get('is_name') }}, terima kasih sudah menjadi bagian dari Espos Plus. Silakan <a href="https://id.solopos.com/subscribe?ref={{ request()->fullUrl() }}" title="Langganan Espos Plus">Aktifkan Paket <i class="fa fa-arrow-circle-o-right"></i></a> berlangganan untuk membaca artikel ini dan dapatkan berbagai konten menarik di Espos Plus.
													</div>
												</div>
												<div class="button-container">
													<a href="https://id.solopos.com/subscribe?ref={{ request()->fullUrl() }}" title="langganan" class="checkout-button" style="padding:8px 10px !important; font-size:12px;">Langganan Sekarang, Dapat Mobil !</a>
													<a href="https://www.solopos.com/page/help" target="_blank">PELAJARI SELENGKAPNYA</a>
												</div>
												<div class="footer-box">
													<span class="callout">*Syarat & Kententuan berlaku</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div>
									<a href="https://id.solopos.com/login/"><img src="https://cdn.solopos.com/plus/espos-plus-desktop.jpg" width="100%"></a>
								</div>
								<div class="gap-30"></div>
								@else
								{!! $konten !!}
								@endif
							</div>
							@else
							<div id="promosi">
								{!! implode('</p>', $intro) !!}
								<div style="background-image: linear-gradient(
								180deg,hsla(0,0%,100%,0),#fff);height:5rem;width:100%;position:relative;margin-top:-90px;"></div>
								<div class="subscribe mx-auto">
								  <div class="row justify-content-center">
									<div class="col-md-12">
									  <div class="header-box">
										<img class="logo" src="{{ url('images/plus.png') }}" alt="logo">
										<div class="login">
										  Sudah Langganan ?  <a href="https://id.solopos.com/login?ref={{ request()->fullUrl() }}" title="login">Login <i class="fa fa-arrow-circle-o-right"></i></a>
										</div>
									  </div>

									  <div class="top-box">
										<div class="lead">Lanjutkan Membaca...</div>
										<div class="sublead">Silakan berlangganan untuk membaca artikel ini dan dapatkan berbagai konten menarik di Espos Plus.</div>
									  </div>
									  <div class="button-container">
										<a href="https://id.solopos.com/subscribe?ref={{ request()->fullUrl() }}" title="langganan" class="checkout-button" style="padding:8px 10px !important; font-size:12px;color:#fff;">Langganan Sekarang, Dapat Mobil !</a>
										<a href="https://id.solopos.com/login?ref={{ request()->fullUrl() }}" title="Login" class="checkout-button inverse">LOGIN | DAFTAR</a>
										<a href="https://www.solopos.com/page/help" target="_blank">PELAJARI SELENGKAPNYA</a>
									  </div>
									  <div class="footer-box">
										<span class="callout">*Syarat & Kententuan berlaku</span>
										</div>
									</div>
								  </div>
								</div>
							</div>
							<div>
								<a href="https://id.solopos.com/login/"><img src="https://cdn.solopos.com/plus/espos-plus-desktop.jpg" width="100%"></a>
							</div>
							<div class="gap-30"></div>
							@endif
						</div><!-- post-content-area end -->

				        <div class="container social-share-btn d-flex align-items-center flex-wrap">
				          SHARE :
				          <a class="btn-facebook" href="https://www.facebook.com/sharer/sharer.php?u={{ $header['link'] }}" target="_blank" rel="noopener" title="social"><i class="fa fa-facebook"></i></a>
				          <a class="btn-twitter" href="https://twitter.com/home?status={{ $header['link'] }}" target="_blank" rel="noopener" title="social"><i class="fa fa-twitter"></i></a>
				          <a class="btn-instagram" href="https://www.instagram.com/koransolopos" title="social"><i class="fa fa-instagram"></i></a>
				          <a class="btn-whatsapp" href="https://web.whatsapp.com/send?text=*{{ urlencode($header['title']) }}* | {{ $header['ringkasan'] }} |  _selengkapnya baca di sini_ {{ $header['link'] }}" target="_blank" rel="noopener" title="social"><i class="fa fa-whatsapp"></i></a>
				          <a class="btn-quora" href="mailto:?subject=Artikel Menarik dari Solopos.com tentang &amp;body=Artikel ini sangat berguna bagi Anda, silahkan klik link berikut ini"><i class="fa fa-envelope"></i></a>
				        </div>

						<div class="post-footer">
							<div class="gap-30"></div>
							<div class="tag-lists">
								<span>Tags: </span>
				                @if(isset($data['tags']['tag']))
                                    @foreach($data['tags']['tag'] as $tag)
                                    @php
                                        $tag_name = $tag;
                                        $tag_slug = str_replace(' ', '-',$tag)
                                    @endphp
                                        <a href="{{ url("/tag/{$tag_slug}") }}">{{ $tag_name }}</a>
                                    @endforeach
                                @endif
							</div><!-- tag lists -->
						</div>

					</div><!-- single-post end -->

					<div class="gap-30"></div>

					<div class="row">
						<!-- Block Konten Premium -->
						<div class="block style2 text-light mb-20 mt-10">
							<h2 class="block-title">
								<span class="title-angle-shap"> Espos Plus </span>
							</h2>

							<div class="row">
					            @php $pc_loop = 1; @endphp
					            @foreach($premium as $pc) @if($pc_loop <= 5)

					            @if($pc_loop == 1)
								<div class="col-lg-6 col-md-6">
									<div class="post-block-style">
										<div class="post-thumb">
											<a href="{{ url("/{$pc['slug']}-{$pc['id']}") }}?utm_source=premium_desktop" title="{{ html_entity_decode($pc['title']) }}">
												<img src="{{ $pc['images']['thumbnail'] }}" alt="{{ html_entity_decode($pc['title']) }}" style="object-fit: cover; object-position: center; width: 366px; height: 178px;">
											</a>
										</div>

										<div class="post-content mt-3">
											<h2 class="post-title title-md">
												<span class="espos-plus">+ PLUS</span>
												<a href="{{ url("/{$pc['slug']}-{$pc['id']}") }}?utm_source=terkini_desktop" title="{{ html_entity_decode($pc['title']) }}">{{ html_entity_decode($pc['title']) }}</a>
											</h2>
											<p>@if($pc['summary']) {!! $pc['summary'] !!} @endif</p>
											<div class="post-meta mb-7">
												<span class="post-author"><a href="#"><i class="fa fa-user"></i> @if($pc['author']) {!! $pc['author'] !!} @endif</a></span>
												<span class="post-date"><i class="fa fa-clock-o"></i> {{ Helper::time_ago($pc['date']) }}</span>
											</div>
										</div><!-- Post content end -->
									</div><!-- Post block a end -->
								</div><!-- Col 1 end -->

								<div class="col-lg-6 col-md-6">
									<div class="row ts-gutter-20">
								@endif

                                @if( $pc_loop > 1 && $pc_loop <= 5 )

										<div class="col-md-6">
											<div class="post-block-style">
												<div class="post-thumb">
													<a href="{{ url("/{$pc['slug']}-{$pc['id']}") }}?utm_source=terkini_desktop" title="{{ $pc['title'] }}">
														<img src="{{ $pc['images']['thumbnail'] }}" alt="{{ $pc['title'] }}" style="object-fit: cover; object-position: center; width: 168px; height: 84px;">
													</a>
												</div>

												<div class="post-content">
													<h2 class="post-title mb-2">
														<span class="espos-plus">+ PLUS</span>
														<a href="{{ url("/{$pc['slug']}-{$pc['id']}") }}?utm_source=terkini_desktop" title="{{ html_entity_decode($pc['title']) }}">{{ html_entity_decode($pc['title']) }}</a>
													</h2>
												</div><!-- Post content end -->
											</div><!-- Post block a end -->
										</div><!-- .col -->

								@endif
								@if($pc_loop == 5)
									</div><!-- .row -->
								</div><!-- Col 2 end -->
							@endif
                            @endif
                            @php $pc_loop++; @endphp
                            @endforeach
							</div><!-- Row end -->
						</div><!-- Block Konten Premium end -->
					</div><!-- row end -->
					<!-- realted post end -->

					<div class="gap-50 d-none d-md-block"></div>
					<div>
						<h2 class="block-title">
							<span class="title-angle-shap"> Espos Plus Lainnya </span>
						</h2>
						<div class="row ts-gutter-20 loadmore-frame">
				          @php $no = 0; @endphp
				          @foreach($premium as $ec)
				          @if($no > 5 && $no < 26 )
							<div class="col-12 mb-10 content-box">
								<div class="post-block-style">
									<div class="row">
										<div class="col-md-5">
											<div class="post-thumb post-list_feed">
												<img src="{{ $ec['images']['thumbnail'] }}" alt="{{ $ec['title'] }}" style="object-fit: cover; object-position: center; height: 167px; width: 320px;">
												<a class="post-cat-box {{ $ec['category'] }}" href="{{ url("/{$ec['category']}") }}">{{ $ec['category'] }}</a>
											</div>
										</div>
										<div class="col-md-7 pl-0">
											<div class="post-content">
												<h2 class="post-title title-md">
													<span class="espos-plus">+ PLUS</span> <a href="{{ url("/{$ec['slug']}-{$ec['id']}") }}?utm_source=fokus_desktop" title="{{ html_entity_decode($ec['title']) }}">{{ html_entity_decode($ec['title']) }}</a>
												</h2>
												<div class="post-meta mb-7">
													<span class="post-author"><a href="#"><i class="fa fa-user"></i> @if($ec['author']) {!! $ec['author'] !!} @endif</a></span>
													<span class="post-date"><i class="fa fa-clock-o"></i> {{ Helper::time_ago($ec['date']) }}</span>
												</div>
												<p>@if($ec['summary']) {!! $ec['summary'] !!} @endif</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							@endif
						    @php $no++; @endphp
                            @endforeach
							<div class="col-12 mt-3 align-items-center" style="text-align: center;">
					            <a href="javascript:void(0)" class="btn btn-primary btn-sm load-more" title="Kumpulan Berita">Espos Plus Lainnya...</a>
					            <a href="https://www.solopos.com/arsip-plus" class="btn btn-primary btn-sm load-more-arsip" style="display: none;" title="Kumpulan Berita">Arsip Berita</a>
							</div><!-- col end -->
						</div>
					</div><!-- Content Col end -->
				</div><!-- col-lg-8 -->

				<style>
					.subscribe {
						background-color: #fff;
						margin-top: 20px;
						margin-bottom: 50px;
						position: relative;
						border: 1px solid #00437d;
						border-radius: 10px;
					}
					.header-box {
						display: flex;
						flex-direction: row;
						justify-content: space-between;
						align-items: center;
						padding: 30px 30px 10px;
					}
					.top-box {
						display: flex;
						flex-direction: column;
						justify-content: center;
						padding: 0 30px;
					}
					.header-box .logo {
						width: 150px;
					}
					.header-box .login {
						font-family: Verdana, Geneva, Tahoma, sans-serif;
						font-size: 14px;
						font-weight: 600;
						text-align: center;
						letter-spacing: 0.02em;
						color: #f29b27;
					}
					.subscribe .lead {
						font-size: 27px;
						font-family: Verdana, Geneva, Tahoma, sans-serif;
						line-height: 34px;
						font-weight: 700;
						text-align: center;
						letter-spacing: -0.01em;
						color: #2E2E2E;
						margin: 5px auto 10px auto;
					}
					.subscribe .sublead {
						text-align: center;
						margin: 0 auto 15px auto;
						font-size: 12px;
						font-family: Verdana, Geneva, Tahoma, sans-serif;
						line-height: 21px;
					}
					.button-container {
						display: flex;
						flex-direction: column;
						justify-content: center;
						margin: 20px auto;
					}
					.checkout-button {
						display: inline-block;
						width: 325px;
						height: 40px;
						padding: 8px 32px;
						border: 1px solid #00437d;
						border-radius: 4px;
						font-family: Verdana, Geneva, Tahoma, sans-serif;
						font-weight: bold;
						font-size: 14px;
						letter-spacing: 0.05em;
						text-transform: uppercase;
						text-align: center;
						color: #FFFFFF !important;
						background: #00437d;
						margin: 0 auto 15px auto;
					}
					.inverse {
						color: #00437d !important;
						background: #FFFFFF;
						border: 1px solid #00437d;
					}
					.button-container a {
						font-family: Verdana, Geneva, Tahoma, sans-serif;
						font-size: 14px;
						font-weight: bold;
						text-align: center;
						letter-spacing: 0.02em;
						text-decoration: none;
					}
					.footer-box {
						display: flex;
						flex-direction: row;
						justify-content: flex-start;
						padding: 30px;
					}
					.callout {
						display: block;
						font-family: 'Roboto', sans-serif;
						font-weight: 500;
						font-size: 9px;
						line-height: 9px;
						text-transform: uppercase;
						letter-spacing: 0.05em;
						opacity: 0.7;
					}
					.trending-bar {background: #f29b27;}
				</style>
			</div><!-- row end -->
		</div><!-- container end -->
	</section><!-- category-layout end -->

	{{-- <iframe src="https://api.solopos.com/set-view?id={{ $content['id'] }}" style="position: absolute;width:0;height:0;border:0;bottom:0;"></iframe> --}}
    @push('custom-scripts')
    {{-- <script>
      $(window).load(function() {
        $.ajax({
            type: "GET",
            url: 'https://api.solopos.com/set-view?id={{ $content['id'] }}',
        });
    });
    </script> --}}
    @endpush
    @push('tracking-scripts')
    <script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
    url:'https://tf.solopos.com/api/v1/stats/store',
    method:'POST',
    data:{
            post_id:'{{ $content['id'] }}',
            post_title:'{{ $header['title'] }}',
            post_slug:'{{ $content['slug'] }}',
            post_date:'{{ $content['date'] }}',
            post_author:'{{ $header['author'] }}',
            post_editor:'{{ $header['editor'] }}',
            post_category:'{{ $header['category_parent'] }}',
            post_subcategory:'{{ $header['category_child'] }}',
            post_tag:'{!! serialize($content['tag']) !!}',
            post_thumb:'{{ $header['image'] }}',
            post_view_date:'{{ date('Y-m-d') }}',
            domain:'{{ 'solopos.com' }}'
        },
    success:function(response){
        if(response.success){
            console.log(response.message)
        }else{
            console.log(error)
        }
    },
    error:function(error){
        console.log(error)
    }
    });
    </script>
    @endpush

@endsection
