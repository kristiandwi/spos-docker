@extends('layouts.app')
@section('content')
	<section class="main-content category-layout-1 pt-0">
		<div class="container">
			<div class="post-header-area" align="center">
				<h2 class="post-title title-lg">COPYRIGHT</h2>
				<img class="w-100 single-blog-image" src="{{ url('images/solopos.jpg') }}" alt="Solopos Digital Media">
			</div>	
			
			<div class="gap-30"></div>
			
			<div class="row ts-gutter-30">
				<p>Seluruh konten di <strong>Solopos.com</strong>, <strong>Harianjogja.com</strong>, <strong>Madiunpos.com</strong> dan <strong>Semarangpos.com</strong> baik berupa artikel, berita, teks, foto dan video dilindungi oleh UU Hak Cipta. Informasi yang disajikan Solopos.com Group bisa dimanfaatkan individu untuk referensi dan bersifat nonkomersial.</p>
				<p>Pengguna tidak diperkenankan menyalin, memproduksi ulang, mempublikasikan secara utuh maupun sebagian materi di Solopos.com Group dengan cara apapun dan atau melalui media apapun, kecuali untuk kepentingan pribadi dan tidak bersifat komersial.</p>
				<p>Penggunaan di luar kepentingan tersebut harus mendapat persetujuan tertulis dan kerja sama dengan Solopos.com Group.</p>
				<p>Solopos.com Group menjalin kerja sama dengan beberapa sindikasi berita dan kantor berita, sehingga ada beberapa materi berupa berita, foto, maupun video bersumber dari situs-situs sindikasi dan kantor berita.</p>
			</div><!-- row end -->
			
		</div><!-- container end -->
	</section><!-- category-layout end -->

@endsection