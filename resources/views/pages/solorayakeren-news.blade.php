@extends('layouts.app-solorayakeren')
@section('content')	
      <!--=====================================-->
      <!--=    Inne Page Banner Area Start    =-->
      <!--=====================================-->
      <!-- start inner-banner -->
      <section data-bg-image="https://cdn.solopos.com/tematik/image/banner/inner-banner10.jpg" style="padding-top: 70px;padding-bottom: 50px;">
      </section>
      <!--=====================================-->
      <!--=     Inne Page Banner Area End     =-->
      <!--=====================================-->
      <!--=====================================-->
      <!--=   Blog Grid Section Area Start    =-->
      <!--=====================================-->
      <section class="blog-list-wrap">
        <div class="container">
          <div class="row">
            <div class="col-lg-8">
                <!--div class="row ts-gutter-20 align-items-center loadmore-frame"-->
            @php
	            $loop_no = 1;
	          @endphp
            @foreach ($keren as $posts)
            @php           
            $thumb = $posts['featured_image']['thumbnail'] ?? 'https://www.solopos.com/images/no-thumb.jpg'; 
            $medium = $posts['featured_image']['medium'] ?? 'https://www.solopos.com/images/no-medium.jpg';
            $title = html_entity_decode($posts['title']);
            @endphp
            @if($loop_no<=10)  
            <div class="blog-box-layout1 blog-list-layout wow fadeInUp animated" data-wow-delay="0.3s" data-wow-duration="1s">
                <div class="figure-box figure-top">
                  <a href="{{ url("/{$posts['slug']}-{$posts['id']}") }}" class="link-wrap"><img src="{{ $medium }}" alt="{{ $title }}" width="100%" height="415"></a>
                  <div class="entry-date-wrap">
                    <svg width="90" height="68" viewBox="0 0 90 68" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M90 68C90 67.888 90 67.7573 90 67.6266C90 67.4959 90 67.3839 90 67.2718L90 34.5228L90 33.4585L90 0.728172C90 0.616145 90 0.485448 90 0.373421C90 0.242724 90 0.112026 90 0C89.0048 0.504119 88.1786 0.858868 87.6153 1.08292C83.5781 2.68863 80.1231 3.04338 78.4707 3.02471C77.7571 3.02471 77.0624 3.02471 76.3488 3.02471L0.131431 3.02471C3.66158 6.53487 5.91487 10.0264 9.42625 13.5365L-1.0569e-06 24.179L8.61881 34L-1.91548e-06 43.821L9.42624 54.4635C5.89609 57.9736 3.6428 61.4651 0.131429 64.9753L76.3676 64.9753C77.0812 64.9753 77.7759 64.9753 78.4895 64.9753C80.1419 64.9753 83.5969 65.3114 87.634 66.9171C88.1974 67.1411 89.0236 67.4959 90 68Z" fill="#EE0034" />
                    </svg>
                    <div class="entry-date">{{ Carbon\Carbon::parse($posts['date'])->translatedFormat('j') }}<span>{{ Carbon\Carbon::parse($posts['date'])->translatedFormat('M') }}</span></div>
                  </div>
                </div>
                <div class="content-box">
                  <div class="entry-meta mb-1">
                    <ul>
                      <li><i class="far fa-user"></i> {{ $posts['author'] }}</li>
                      <li><i class="fas fa-tags"></i> {{ $posts['catsname'] }}</li>
                    </ul>
                  </div>
                  <h3 class="entry-title"><a href="{{ url("/{$posts['slug']}-{$posts['id']}") }}">{{ $title }}</a></h3>
                  <p class="entry-description" style="overflow: hidden; display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical;">{{ html_entity_decode($posts['summary']) }}</p>
                </div>
              </div>
            @endif
            @php $loop_no++; @endphp
			      @endforeach
              
            <div class="col-12 mt-3 align-items-center" style="text-align: center;">
                <a href="javascript:void(0)" class="btn btn-primary btn-sm load-more" title="Kumpulan Berita">Lihat Berita Lainnya</a>
                <a href="https://www.solopos.com/arsip" class="btn btn-primary btn-sm load-more-arsip" style="display: none;" title="Kumpulan Berita">Arsip Berita</a>
            </div><!-- col end -->
            <!--/div-->	
            {{--<div class="text-center">
                <ul class="pagination-layout1">
                  <li><a href="{{ url('/solorayakeren') }}"><i class="fas fa-arrow-left"></i></a></li>
                  <li class="active"><a href="{{ url('/solorayakeren') }}">1</a></li>
                  <li><a href="{{ url('/solorayakeren') }}">2</a></li>
                  <li><a href="{{ url('/solorayakeren') }}">3</a></li>
                  <li><a href="{{ url('/solorayakeren') }}">4</a></li>
                  <li><a href="{{ url('/solorayakeren') }}"><i class="fas fa-arrow-right"></i></a></li>
                </ul>
              </div>--}}
            </div>
            
            @include('includes.solorayakeren.sidebar')
          </div>
        </div>
      </section>
      <!--=====================================-->
      <!--=    Blog Grid Section Area End     =-->
      <!--=====================================-->
    <style>
    .load-more-btn {
        position: relative;
        }

    .load-more-btn::after {
        position: absolute;
        left: 0;
        top: 0;
        bottom: 0;
        width: 100%;
        background: #eaeaea;
        height: 1px;
        content: '';
        margin: auto;
        }

    .load-more-btn .btn {
        padding: 10px 55px;
        background: #fff;
        border: 1px solid #eaeaea;
        cursor: pointer;
        font-weight: 700;
        position: relative;
        -webkit-transition: all 0.4s ease;
        -o-transition: all 0.4s ease;
        transition: all 0.4s ease;
        z-index: 1;
        color: #000;
        }

    .load-more-btn .btn:hover {
        color: #00437d;
        }

    .loadmore-frame .content-box {
        display:none;
        }

    </style>
@endsection 