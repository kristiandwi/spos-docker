@extends('layouts.app-amp')
@section('content')
<div class="full-post">
  <div class="news-box-content">
      <h1 class="news-box-content-title">
        {{ $header['title'] }}
      </h1>
      <p class="news-box-content-sub"><em>{{ $header['ringkasan'] }}</em></p>
      <p class="news-box-content-sub">
        <em>{{ Carbon\Carbon::parse($content['date'])->translatedFormat('l, j F Y - H:i') }} WIB</em>
        
        @if($header['author'] != $header['editor'] )
        
        <em class="multi-reporter" style="margin-top:10px;margin-bottom:-7px;">
          <em style="display:inline-block;">Penulis:</em>
        @foreach ($author_slug as $author)
        <a href="@if (ucwords(str_replace('_', ' ', $author)) ==  $header['editor'] ) {{ url('/')}}/author/{{ $header['editor_url'] }}/amp @else {{ url('/') }}/penulis/{{$author}}/amp @endif">{{ ucwords(str_replace('_', ' ', $author)) }}</a>
        @endforeach
        </em>
        @endif
       
        <em style="display:inline-block;">Editor:</em>  <a href="@if(!empty($header['editor_url'])){{ url('/')}}/author/{{ $header['editor_url'] }}/amp @else https://www.solopos.com/arsip @endif"  target="_blank">{{ $header['editor'] }}</a> <em style="display:inline-block;">| Solopos.com</em>
      </p>
  </div>    
  @if(!empty($video))
  <amp-youtube data-videoid="{{ $video }}" layout="responsive" width="480" height="270"></amp-youtube>
  @else
  <amp-img src="{{ $content['image'] }}" alt="{{ html_entity_decode($content['title']) }}" layout="responsive" width="600" height="400"></amp-img>
  <p class="img-caption">
    @if(!empty($content['caption']))
    SOLOPOS.COM - {{ htmlentities($content['caption']) }}
    @else
    SOLOPOS.COM - Panduan Informasi dan Inspirasi
    @endif
  </p> 
  @endif 
  <div class="news-box-content">  

      {{-- <div class="addthis-wrapper">
        <amp-addthis width="320" height="92"  data-pub-id="ra-4dc8efb674777330" data-widget-id="thr1" data-widget-type="inline"></amp-addthis>
      </div> --}}
      <div class="decoration"></div>
      @php
      $konten = Helper::ampify(htmlspecialchars_decode($content['content']));
      $contents = explode('</p>', $konten);
      $total_p = count(array_filter($contents)); 
      $intro  = array_slice($contents, 0, 1);
      @endphp
      
      @if(Cookie::get('is_login') == 'true')
      <div id="konten-premium" class="premium">
            <div class="profile-content-wrapper" style="background:rgb(214, 228, 250);border-radius:5px;padding:7px;">
                <div class="user-meta-data">  
                    <h6 style="margin-top:0;font-size:14px;font-weight:400; margin-bottom: 10px;">
                        <a href="https://id.solopos.com/profile">{{ Helper::greetings() }} <strong>{{ Cookie::get('is_name') }} !</strong></a>
                    </h6>          
                    <div class="user-content" style="padding-left:0;width:70%; float: left;">                   
                        <p>Terimakasih telah bergabung di komunitas SoloposID. Silahkan kelola akun Anda melalui Dashboard SoloposID.</p>                                     
                    </div>
                    <div style="width: 30%; float:left; text-align:center;">
                        <amp-img src="https://id.solopos.com/images/logo.png" height="22" width="180" style="margin-bottom: 5px;"></amp-img>
                        <div class="post-list" style="margin:10px 5px;background:rgb(4, 255, 117)">
                            <a href="https://id.solopos.com/profil" style="color: #fff;"> Akun Saya </a>
                        </div>                         
                </div>                    
                </div>
            </div>								
            {!! $konten !!}								
      </div>	
      @else													
      <div id="promosi">
            {!! implode('</p>', $intro) !!}
            <div style="background-image: linear-gradient(180deg,hsla(0,0%,100%,0),#fff);height:5rem;width:100%;position:relative;margin-top:-90px;"></div>
            <div class="profile-content-wrapper" style="background:rgb(214, 228, 250); text-align:center;border-radius:5px;">
                <amp-img src="{{ url('images/premium.png') }}" height="40" width="180" style="margin-bottom: 20px;"></amp-img>
                <div class="user-meta-data">            
                    <div class="user-content" style="padding-left:0;">                   
                        <p>Espos Premium merupakan layanan khusus dari Solopos.com yang lebih relevan dan memiliki diferensiasi dibandingkan free content. Untuk membaca artikel ini selengkapnya silahkan login atau daftar di SoloposID.</p>
                        <div class="post-list" style="margin:10px 5px;padding:10px 15px;background:rgba(3, 158, 248, 0.959)">
                            <a href="https://id.solopos.com/login?ref={{ request()->fullUrl() }}"> LOGIN </a>
                        </div> 
                        <div class="post-list" style="margin:10px 5px;padding:10px 15px;background:rgb(255, 75, 4)">
                            <a href="https://id.solopos.com/register"> DAFTAR </a>
                        </div>                                     
                    </div>
                </div>
            </div>
      </div>
      @endif     
      <div class="container mb-5 center-text">
        <amp-social-share type="facebook" width="64" height="24" class="facebook-bg"></amp-social-share>
        <amp-social-share type="twitter" width="64" height="24" class="twitter-bg"></amp-social-share>
        <amp-social-share type="whatsapp" width="64" height="24" class="whatsapp-bg"></amp-social-share>
        <amp-social-share type="email" width="64" height="24" class="mail-bg"></amp-social-share>
      </div>
      <div class="decoration"></div>
      <div class="container mt-3">
        <div class="tag-lists">
        <span>Kata Kunci :</span>
        @if(isset($content['tag']))
            @foreach($content['tag'] as $tag)
            @php
                $tag_name = ucwords($tag);
                $tag_slug = str_replace(' ', '-',$tag);
                $tag_slug = strtolower($tag_slug)
            @endphp
            <a href="{{ url("/tag/{$tag_slug}") }}/amp">{{ $tag_name }}</a>
            @endforeach
        @endif                                
        </div>
      </div>
            
      <!-- start hanya untukmu -->
      <h2 class="uppercase full-top no-bottom">Hanya Untuk Anda</h2>
      <h6 class="uppercase full-bottom color-green-dark">Inspiratif & Informatif</h6>       
      <div class="news-top half-bottom">
        @php $hl_loop = 1; @endphp
        @foreach($premium as $hl)
        @if($hl_loop <= 15)       
        <a href="{{ url("/{$hl['slug']}-{$hl['id']}") }}/amp" title="{{ $hl['title'] }}" class="news-header">
            <amp-img src="{{ $hl['images']['thumbnail'] }}" layout="responsive" width="600" height="400" alt="{{ $hl['title'] }}"></amp-img>
            <u class="bg-green-dark">+ PLUS</u>
            <i><span>{{ $hl['category'] }}</span></i>
            <strong>{{ $hl['title'] }}</strong>
            <em>{{ Helper::time_ago($hl['date']) }}</em>
        </a>
        <amp-accordion class="news-share">
            <section>
                <h4><i class="fa fa-retweet"></i></h4>
                <p>
                    <amp-social-share type="facebook" width="43" height="40" class="custom-news-share"><i class="fa fa-facebook"></i></amp-social-share>
                    <amp-social-share type="twitter" width="43" height="40" class="custom-news-share"><i class="fa fa-twitter"></i></amp-social-share>
                    <amp-social-share type="pinterest" width="43" height="40" class="custom-news-share"><i class="fa fa-pinterest"></i></amp-social-share>
                    <amp-social-share type="linkedin" width="43" height="40" class="custom-news-share"><i class="fa fa-linkedin"></i></amp-social-share>
                    <amp-social-share type="email" width="43" height="40" class="custom-news-share"><i class="fa fa-envelope-o"></i></amp-social-share>
                </p>
            </section>
        </amp-accordion>
        @endif
        @php $hl_loop++ @endphp
        @endforeach

    </div>	         
  </div> <!-- end blog content -->
</div>
{{-- <amp-img src="https://api.solopos.com/set-view?id={{ $content['id'] }}" width="1" height="1" alt="view"></amp-img> --}}
@endsection