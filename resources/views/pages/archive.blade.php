@extends('layouts.app')
@section('content')

@include('includes.ads.popup-banner')
@include('includes.ads.wrapt')
	<!-- breadcrumb -->
	<div class="breadcrumb-section">
		<div class="container ">
			<div class="row">
				<div class="col-12">
					<ol class="breadcrumb">
						<li>
							<a href="{{ url('/') }}"><i class="fa fa-home"></i></a>
							<i class="fa fa-angle-right"></i> {{ $header['title'] }}
						</li>
						{{-- <li style="text-transform: capitalize;">
						    <i class="fa fa-angle-right"></i>
						</li> --}}
					</ol>
				</div>
			</div><!-- row end -->
		</div><!-- container end -->
	</div>
	<!-- breadcrumb end -->

	<section class="main-content pt-0">
		<div class="container pl-0 pr-0">
			<div class="row ts-gutter-30">
				<div class="col-lg-8">
					<h2 class="block-title">
						<span class="title-angle-shap"> Update "{{ $header['title'] }}" </span>
					</h2>
					<div class="row ts-gutter-20 align-items-center">
						@php
	                        $loop_no = 1;
	                    @endphp
                        @foreach ($data as $post)
						@php
						$thumb = $post['featured_image']['thumbnail'] ?? 'https://www.solopos.com/images/no-thumb.jpg';
            			$medium = $post['featured_image']['medium'] ?? 'https://www.solopos.com/images/no-thumb.jpg';
						$title = html_entity_decode($post['title']);
						@endphp
                            <div class="col-12 mb-10">
                                <div class="post-block-style">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="post-thumb post-list_feed">
                                                <img src="{{ $thumb }}" alt="{{ $title }}" onerror="javascript:this.src='https://www.solopos.com/images/no-thumb.jpg'">
                                                <a class="post-cat-box cekfakta" href="{{ url("/cekfakta") }}">Cek Fakta</a>
                                            </div>
                                        </div>

                                        <div class="col-md-7 pl-0">
                                            <div class="post-content">
												@if($loop_no==1)
												<h1 class="post-title title-md">
													{{--@if($post['konten_premium'] == 'premium')
													<span class="espos-plus">+ PLUS</span>
													@endif	--}}
													<a href="{{ url("/{$post['slug']}-{$post['id']}") }}?utm_source=terkini_desktop" title="{{ html_entity_decode($post['title']) }}">{{ html_entity_decode($post['title']) }}</a>
												</h1>
												@else
												<h2 class="post-title title-md">
													{{-- @if($hl['konten_premium'] == 'premium')
													<span class="espos-plus">+ PLUS</span>
													@endif --}}
													<a href="{{ url("/{$post['slug']}-{$post['id']}") }}?utm_source=terkini_desktop" title="{{ html_entity_decode($post['title']) }}">{{ html_entity_decode($post['title']) }}</a>
												</h2>
												@endif
                                                <div class="post-meta mb-7">
                                                    <span class="post-author"><a href="#"><i class="fa fa-user"></i> @if($post['author']) {!! $post['author'] !!} @endif</a></span>
                                                    <span class="post-date"><i class="fa fa-clock-o"></i> {{ Helper::time_ago($post['date']) }}</span>
                                                </div>
                                                <p>@if($post['summary']) {!! htmlspecialchars_decode($post['summary']) !!} @endif</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						@endforeach
					</div>
				</div><!-- col-lg-8 -->

				<!-- sidebar start -->
				@include('includes.sidebar-category')
				<!-- sidebar end -->
			</div><!-- row end -->
		</div><!-- container end -->
	</section><!-- category-layout end -->
@endsection
