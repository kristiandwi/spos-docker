@if( date('Y-m-d H:i:s') >= '2022-04-21 07:00:01' && date('Y-m-d H:i:s') <= '2022-04-22 06:59:59')
<div class="container pl-0 pr-0" style="background: url(https://cdn.solopos.com/iklan/live-desktop.png);background-size: 100% 300px;height:300px;position:relative;">
    <a href="https://www.youtube.com/watch?v=ZjYKWIVo3tw" target="_blank" title="Live Solopos">
        <div style="width:580px;height:300px;position:absolute;left:0;top:0;z-index:2;">
            <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
            <script>
            window.googletag = window.googletag || {cmd: []};
            googletag.cmd.push(function() {
                googletag.defineSlot('/54058497/Desktop-LIVE', [580, 300], 'div-gpt-ad-1650348661067-0').addService(googletag.pubads());
                googletag.pubads().enableSingleRequest();
                googletag.enableServices();
            });
            </script>
            <!-- /54058497/Desktop-LIVE -->
            <div id='div-gpt-ad-1650348661067-0' style='min-width: 580px; min-height: 300px;'>
            <script>
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1650348661067-0'); });
            </script>
            </div>
        </div>
    </a>
    <div style="width:380px;height:215px;position:absolute;right:10px;bottom:10px;z-index:3;">
        <iframe width="380" height="214" src="https://www.youtube.com/embed/ZjYKWIVo3tw?&autoplay=1&mute=1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>
@endif