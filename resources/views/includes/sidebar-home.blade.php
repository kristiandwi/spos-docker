<!-- sidebar start -->
<div class="col-lg-4">
	<div class="sidebar">
		@include('includes.ads.desktop-sidebar-1')
		
		@include('includes.widget-popular-all')

		<style>
			.ads-image iframe {width: 300px !important;}
		</style>
		@include('includes.ads.desktop-sidebar-2')

		@include('includes.widget')

		@include('includes.ads.uksw-sidebar')
			
		<div class="sidebar-widget">
			<h2 class="block-title block-title-dark">
				<span class="title-angle-shap"> News </span>
			</h2>
			<div class="list-post-block">
				<ul class="list-post">
		          @php $bn_loop = 1; @endphp
		          @foreach($news as $bn) @if($bn_loop <= 2)
					<li>
						<div class="post-block-style media">
							<div class="post-thumb">
								<a href="{{ url("/{$bn['slug']}-{$bn['id']}") }}?utm_source=sidebar_desktop" title="{{ html_entity_decode($bn['title']) }}">
									<img loading="lazy" class="img-fluid" src="{{ $bn['images']['url_thumb'] }}" alt="{{ html_entity_decode($bn['title']) }}" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
								</a>
							</div><!-- Post thumb end -->

							<div class="post-content media-body">
								<h2 class="post-title">
									{{-- @if($bn['konten_premium'] == 'premium')
									<span class="espos-plus">+ PLUS</span>
									@endif --}}
									<a href="{{ url("/{$bn['slug']}-{$bn['id']}") }}?utm_source=sidebar_desktop" title="{{ html_entity_decode($bn['title']) }}">{{ html_entity_decode($bn['title']) }}</a>
								</h2>
								<div class="post-meta mb-7">
									<span class="post-date"><i class="fa fa-clock-o"></i> {{ Helper::time_ago($bn['date']) }}</span>
								</div>
							</div><!-- Post content end -->
						</div><!-- Post block style end -->
					</li><!-- Li 1 end -->
					@endif
                    @php $bn_loop++; @endphp
                    @endforeach
				</ul><!-- List post end -->
			</div>
		</div>

		@include('includes.ads.desktop-sidebar-3')

		<div class="sidebar-widget">
			<h2 class="block-title block-title-dark">
				<span class="title-angle-shap"> Lifestyle </span>
			</h2>
			<div class="list-post-block">
				<ul class="list-post">
		          @php $ls_loop = 1; @endphp
		          @foreach($lifestyle as $ls) @if($ls_loop <= 3)
					<li>
						<div class="post-block-style media">
							<div class="post-thumb">
								<a href="{{ url("/{$ls['slug']}-{$ls['id']}") }}?utm_source=sidebar_desktop" title="{{ html_entity_decode($ls['title']) }}">
									<img loading="lazy" class="img-fluid" src="{{ $ls['images']['url_thumb'] }}" alt="{{ html_entity_decode($ls['title']) }}" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
								</a>
							</div><!-- Post thumb end -->

							<div class="post-content media-body">
								<h2 class="post-title">
									{{-- @if($ls['konten_premium'] == 'premium')
									<span class="espos-plus">+ PLUS</span>
									@endif --}}
									<a href="{{ url("/{$ls['slug']}-{$ls['id']}") }}?utm_source=sidebar_desktop" title="{{ html_entity_decode($ls['title']) }}">{{ html_entity_decode($ls['title']) }}</a>
								</h2>
								<div class="post-meta mb-7">
									<span class="post-date"><i class="fa fa-clock-o"></i> {{ Helper::time_ago($ls['date']) }}</span>
								</div>
							</div><!-- Post content end -->
						</div><!-- Post block style end -->
					</li><!-- Li 1 end -->
					@endif
                    @php $ls_loop++; @endphp
                    @endforeach
				</ul><!-- List post end -->
			</div>
		</div>

		@include('includes.ads.desktop-sidebar-4')

		<div class="sidebar-widget">
			<h2 class="block-title block-title-dark">
				<span class="title-angle-shap"> Ekbis </span>
			</h2>
			<div class="list-post-block">
				<ul class="list-post">
		          @php $bs_loop = 1; @endphp
		          @foreach($bisnis as $bs) @if($bs_loop <= 3)
					<li>
						<div class="post-block-style media">
							<div class="post-thumb">
								<a href="{{ url("/{$bs['slug']}-{$bs['id']}") }}?utm_source=sidebar_desktop" title="{{ html_entity_decode($bs['title']) }}">
									<img loading="lazy" class="img-fluid" src="{{ $bs['images']['url_thumb'] }}" alt="{{ html_entity_decode($bs['title']) }}" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
								</a>
							</div><!-- Post thumb end -->

							<div class="post-content media-body">
								<h2 class="post-title">
									{{-- @if($bs['konten_premium'] == 'premium')
									<span class="espos-plus">+ PLUS</span>
									@endif --}}
									<a href="{{ url("/{$bs['slug']}-{$bs['id']}") }}?utm_source=sidebar_desktop" title="{{ html_entity_decode($bs['title']) }}">{{ html_entity_decode($bs['title']) }}</a>
								</h2>
								<div class="post-meta mb-7">
									<span class="post-date"><i class="fa fa-clock-o"></i> {{ Helper::time_ago($bs['date']) }}</span>
								</div>
							</div><!-- Post content end -->
						</div><!-- Post block style end -->
					</li><!-- Li 1 end -->
					@endif
                    @php $bs_loop++; @endphp
                    @endforeach
				</ul><!-- List post end -->
			</div>
		</div>

	</div>
</div><!-- Sidebar Col end -->
