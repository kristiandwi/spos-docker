<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;

class CategoryController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $url = request()->getHost();
        $domain = explode('.', $url);
        $regional = $domain[0];
        $regional_list = $request->segment(1);
        // $regional = $request->subdomain;

        if(empty($regional) OR $regional == "www") {

            $list_cat = array('news', 'soloraya', 'lifestyle', 'jatim', 'otomotif', 'entertainment', 'bisnis', 'sport', 'jateng', 'jogja', 'teknologi', 'video', 'writing-contest', 'loker','cekfakta','jagad-jawa','foto','videos','espospedia','foto', 'pojokbisnis', 'kolom', 'fiksi','sekolah');
            $cat = $request->segment(1);
            $amp = $request->segment(2); // AMP slug on 2nd segment URL

            if(!in_array($cat, $list_cat)) {
                abort(400);
            }

        } else {
            $cat = $regional;
            $amp = $request->segment(1);
        }

        if($regional_list == 'sekolah'){
            return redirect('https://sekolah.solopos.com/');
        }

        // // Redirect when on mobile device
        // if(Helper::mobile_detect()) {
        //     return redirect()->away(Config::get('app.mobile_url').'/'.$cat);
        // }

        $xmlPath = Config::get('xmldata.breaking');
        $xmlPath2 = Config::get('xmldata.topic');
        $story = Helper::read_xml($xmlPath, 'breaking-story');
        $headline = Helper::read_xml($xmlPath, 'breaking-headline');
        $breaking = Helper::read_xml($xmlPath, 'breaking-all');
        $breakingcat = Helper::read_xml($xmlPath, 'breaking-'.$cat);
        $premium = Helper::read_xml($xmlPath, 'breaking-premium');
        //$popular = Helper::read_xml($xmlPath, 'breaking-popular');
        $editorchoice = Helper::read_xml($xmlPath, 'breaking-editor-choice');
        $kolom = Helper::read_xml($xmlPath, 'breaking-kolom');
        $espospedia = Helper::read_xml($xmlPath, 'breaking-espospedia');
        $jateng = Helper::read_xml($xmlPath, 'breaking-jateng');
        $jatim = Helper::read_xml($xmlPath, 'breaking-jatim');
        $jogja = Helper::read_xml($xmlPath, 'breaking-jogja');
        $otomotif = Helper::read_xml($xmlPath, 'breaking-otomotif');
        $espospedia = Helper::read_xml($xmlPath, 'breaking-espospedia');
        $video = Helper::read_xml($xmlPath, 'breaking-videos');
        $bola = Helper::read_xml($xmlPath, 'breaking-sport');
        $news = Helper::read_xml($xmlPath, 'breaking-news');
        $foto = Helper::read_xml($xmlPath, 'breaking-foto');
        $bisnis = Helper::read_xml($xmlPath, 'breaking-bisnis');
        $loker = Helper::read_xml($xmlPath, 'breaking-loker');
        $lifestyle = Helper::read_xml($xmlPath, 'breaking-lifestyle');
        $uksw = Helper::read_xml($xmlPath2, 'uksw');
        //$widget = Helper::read_xml($xmlPath2, 'Ekspedisi-Ekonomi-Digital-2021');
        $datawidget = Http::get('https://api.solopos.com/api/breaking/tag/posts?tags=781384');
        $widget = $datawidget->json();

        // $res_datapopular = Http::get('https://tf.solopos.com/api/v1/stats/popular/all/');
        // $datapopular = $res_datapopular->json();
        // $popular = $datapopular['data'];

        if(!empty($regional) AND $regional !== 'www') {
            $view = 'regional.category';
        } else {
            $view = 'pages.category';
        }

        if($cat == 'videos') {
            $view = 'pages.video';
        }
        if($cat == 'video') {
            $view = 'pages.video';
        }
        if($cat == 'foto') {
            $view = 'pages.foto';
        }
        if($cat == 'espospedia') {
            $view = 'pages.espospedia';
        }

        $catTitle = ucwords($cat);
        $header = array(
            'title' => 'Berita ' .$catTitle. ' terbaru, Berita ' .$catTitle. ' hari ini, Info ' .$catTitle.' terkini',
            'category' => $catTitle,
            'category_parent' => $catTitle,
            'is_premium' => '',
            'focusKeyword' => 'Berita '.$catTitle.' Terbaru Hari ini',
            'description' => 'Berita ' .$catTitle. ' terbaru, Berita ' .$catTitle. ' hari ini, Info ' .$catTitle.' terkini',
            'link'  => 'https://www.solopos.com/'.$cat,
            'image' => 'https://www.solopos.com/images/solopos.jpg',
            'editor' => 'Solopos.com',
            'author' => 'Solopos.com',
            'keyword' => 'Berita ' .$catTitle. ' terbaru, Berita ' .$catTitle. ' hari ini, Info ' .$catTitle.' terkini',
            'news_keyword' => 'Berita ' .$catTitle. ' terbaru, Berita ' .$catTitle. ' hari ini, Info ' .$catTitle.' terkini',
        );

        if(!empty($amp)) {
            $view = 'pages.amp-category';
        }
        $cat2 = lcfirst($cat);
        //dd($cat2);
        return view($view, [
            'story' => $story,
            'category' => $cat2,
            'headline' => $headline,
            'breaking' => $breaking,
            'breakingcat' => $breakingcat,
            'premium' => $premium,
            // 'popular' => $popular,
            'editorchoice' => $editorchoice,
            'news' => $news,
            'bola' => $bola,
            'lifestyle' => $lifestyle,
            'bisnis' => $bisnis,
            'loker' => $loker,
            'kolom' => $kolom,
            'espospedia' => $espospedia,
            'video' => $video,
            'foto' => $foto,
            'jateng' => $jateng,
            'jatim' => $jatim,
            'uksw' => $uksw,
            'widget' => $widget,
            'jogja' => $jogja,
            'otomotif' => $otomotif,
            'header' => $header,
            'subdomain' => ucfirst($regional)
        ]);
    }
}
