<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;


class BugarController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $slug = $request->segment(1);
        $subSlug = $request->segment(2);

        // Redirect when on mobile device
        // if(Helper::mobile_detect() && !empty($subSlug)) {
        //     return redirect()->away(Config::get('app.mobile_url').'/'.$slug.'/'.$subSlug);
        // } else {
        //     return redirect()->away(Config::get('app.mobile_url').'/'.$slug);
        // }

        $xmlPath = Config::get('xmldata.topic');
        $xmlPathBreak = Config::get('xmldata.breaking');

        //$headline = Http::get('https://api.solopos.com/api/breaking/posts?category=750705');
        $berita = Http::get('https://api.solopos.com/api/breaking/posts?category=750705,750871,750873,750875');
        $beritaartikel = Http::get('https://api.solopos.com/api/breaking/posts?category=750871');
        $beritafoto = Http::get('https://api.solopos.com/api/breaking/posts?category=750873');
        $beritagrafis = Http::get('https://api.solopos.com/api/breaking/posts?category=750875');
        $beritapromo = Http::get('https://api.solopos.com/api/breaking/posts?category=763582');
        $popular = Helper::read_xml($xmlPathBreak, 'breaking-popular');
        $lifestyle = Helper::read_xml($xmlPathBreak, 'breaking-lifestyle');
        $story = Helper::read_xml($xmlPathBreak, 'breaking-story');

        $xmlObject = simplexml_load_file('https://www.youtube.com/feeds/videos.xml?channel_id=UCEg-iQZl57dAI-I6lOjWrQw');
        $json = json_encode($xmlObject);
        $phpArray = json_decode($json, true);
        //dd($phpArray);
        $video = $phpArray['entry'];
        //dd($yid);
        $headline = $berita->json();
        $artikel = $beritaartikel->json();
        $foto = $beritafoto->json();
        $grafis = $beritagrafis->json();
        $promo = $beritapromo->json();

        $is_bugar = 'yes';

        $view = 'pages.bugar';
        $title = 'Bugar - Solopos.com';

        if($slug == 'rsjihsolo') {
            if( $subSlug == 'artikel' ) {
                $title = 'Artikel Bugar - Solopos.com';
                $view = 'pages.bugar-artikel';
            }
            if( $subSlug == 'foto' ) {
                $title = 'Berita Foto Bugar - Solopos.com';
                $view = 'pages.bugar-foto';
            }
            if( $subSlug == 'grafis' ) {
                $title = 'Infografis Bugar - Solopos.com';
                $view = 'pages.bugar-grafis';
            }
            if( $subSlug == 'video' ) {
                $title = 'Bugar Video by RS JIH Solo - Solopos.com';
                $view = 'pages.bugar-video';
            }
            if( $subSlug == 'promo' ) {
                $title = 'Promo RS JIH Solo - Solopos.com';
                $view = 'pages.bugar-promo';
            }
            if( $subSlug == 'kontak' ) {
                $title = 'Kontak RS JIH SOLO - Solopos.com';
                $view = 'pages.bugar-kontak';
            }
        } else {
            abort(404);
        }

        $header = array(
            'title' => $title,
            'description' => 'Menyajikan berita terpopuler hari ini, berita trending Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'link'  => 'https://www.solopos.com/rsjihsolo',
            'category' => 'Bugar',
            'category_parent' => 'RSJIH Solo',
            'is_premium' => '',
            'focusKeyword' => 'Bugar',
            'image' => 'https://www.solopos.com/images/solopos.jpg',
            'editor' => 'Solopos.com',
            'author' => 'Solopos.com',
            'keyword' => 'Berita, Terkini, trending, terpopuler, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'news_keyword' => 'Berita, Terkini, trending, terpopuler, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
        );

        return view($view, ['berita' => $berita, 'headline' => $headline, 'artikel' => $artikel, 'grafis' => $grafis, 'foto' => $foto, 'video' => $video, 'promo' => $promo, 'is_bugar' => $is_bugar, 'popular' => $popular, 'lifestyle' => $lifestyle,  'story' => $story, 'header' => $header]);

    }
}
