<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;

class ReadController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, $slug)
    {
        $url = request()->getHost();
        $domain = explode('.', $url);
        $subdomain = $domain[0];
        // $subdomain = $request->subdomain;
        $slug = $request->slug;
        // Redirect when on mobile device
        // if(Helper::mobile_detect()) {
        //     return redirect()->away(Config::get('app.mobile_url').'/'.$request->segment(1));
        // }

        // $uri = $request->segments();
        // dd($uri[0]);
        // $id = $request->segment(1);
        $amp = $request->segment(2); // AMP slug on 2nd segment URL
        $postid = explode('-', $slug);
        $xmlPath = Config::get('xmldata.posts');
        $xmlString = $xmlPath.end($postid).'.xml';

        // $isExists = get_headers($xmlString);

        $isExists = Http::get($xmlString);

        // dd($isExists->status());

        // if($isExists[0] !== "HTTP/1.1 200 OK") {
        if($isExists->status() !== 200) {
            // abort(404);
            $res = Http::get('https://api.solopos.com/api/wp/v2/posts/'.end($postid));
            $data = $res->json();
            $resUser = Http::get('https://api.solopos.com/api/data/user?fullname='.$data['one_call']['post_author']['display_name']);
            $dataUser = $resUser->json();
            // dd($dataUser);
            $cat_list = $data['one_call']['categories_list'][0]['slug'] ?? 'news';
            // if($subdomain !== $cat_list) {
            //     abort(404);
            // }
            $premium_content = $data['one_call']['postmeta']['konten_premium'][0] ?? '';
            // $data = Helper::read_xml($xmlPath, end($postid));
            //dd($premium_content);
            if(!empty($data['tags'])):
                // get related tag id
                foreach($data['one_call']['tags_list'] as $tag){
                    $tag_id[] = $tag['term_taxonomy_id'];
                }
                $tags = implode(',', $tag_id);//(string) $tag_id[0];
                foreach($data['one_call']['tags_list'] as $tag){
                    $tag_name[] = $tag['name'];
                }
            else:
                $tags = '';
                $tag_name = array();
            endif;
            if (empty($data['one_call']['featured_list'])):
                $file_img = 'https://www.solopos.com/images/solopos.jpg';
            else:
                $file_img = $data['one_call']['featured_list']['source_url'];
            endif;

            //dd($file_img);
            $img_headers = @get_headers($file_img);
            if($img_headers[0] == 'HTTP/1.1 404 Not Found') {
                $image = 'https://www.solopos.com/images/solopos.jpg';
            }
            else {
                $image = $file_img;
            }

            if (empty($data['one_call']['post_author']['avatar_url'])):
                $avatar_url = 'https://www.solopos.com/images/solopos.jpg';
            else:
                $avatar_url = $data['one_call']['post_author']['avatar_url'];
            endif;


            // $avatar_headers = @get_headers($avatar_url);
            // if($avatar_headers[0] == 'HTTP/1.1 404 Not Found') {
            //     $avatar = 'https://images.solopos.com/2021/02/avatar-100x100.png';
            // }
            // else {
                $avatar = $avatar_url;
            // }

            $content = [
                'id' => $data['id'],
                'date' => $data['date'],
                'title' => $data['title']['rendered'],
                'summary' => $data['content']['summary'] ?? '',
                'content' => $data['content']['rendered'],
                'slug' => $data['slug'],
                'image' => $image,
                'caption' => $data['one_call']['featured_list']['caption'] ?? 'Solopos Digital Media - Panduan Informasi dan Inspirasi',
                //'image' => 'https://dev.solopos.com/images/solopos.jpg',
                //'caption' => 'Solopos Digital Media',
                'category' => $cat_list,
                'category_parent' => $cat_list,
                'category_child' => $cat_list,
                'tag' => $tag_name,
                'author' => $data['one_call']['postmeta']['sumberlain'][0] ?? 'Redaksi Solopos.com',
                'editor' => $data['one_call']['post_author']['display_name'],
                'avatar' => $avatar,
                'editor_url' => $dataUser['slug'],
                'source' => '',
            ];
            // properties
            // $file = getimagesize($content['image']);
            // $width = $file[0];
            // $height = $file[1];



            $premium_content = $data['properties']['konten_premium'] ?? '';

            $video = $data['properties']['post_url_video'] ?? '';

            $relatedTags = $tags;
            //dd($relatedTags);
            //$breaking_id = Http::get('https://api.solopos.com/api/wp/v2/posts?categories='.$data['categories'][0])->json();

            $breaking_id = Helper::read_xml(Config::get('xmldata.breaking'), 'breaking-all');

        } else {

            $xmlObject = simplexml_load_file($xmlString);

            $json = json_encode($xmlObject);
            $phpArray = json_decode($json, true);

            $data = $phpArray['posts'];
            // $data = Helper::read_xml($xmlPath, end($postid));

            $premium_content = $data['properties']['konten_premium'] ?? '';

            $video = $data['properties']['post_url_video'] ?? '';

            // get related tag id
            foreach($xmlObject->posts->tags->tag as $tag){
                $tags[] = $tag['id'];
            }
            $relatedTags = implode(',', $tags); //(string) $tags[0];

            // dd($relatedTags);

            $file_img = $data['images']['content'];
            // $img_headers = @get_headers($file_img);
            // if($img_headers[0] == 'HTTP/1.1 404 Not Found') {
            //     $image = 'https://www.solopos.com/images/solopos.jpg';
            // }
            // else {
                $image = $file_img;
            // }

            $avatar_url = $data['authors']['avatar'];
            // $avatar_headers = @get_headers($avatar_url);
            // if($avatar_headers[0] == 'HTTP/1.1 404 Not Found') {
            //     $avatar = 'https://images.solopos.com/2021/02/avatar-100x100.png';
            // } else {
                $avatar = $avatar_url;
            // }
            if($data['authors']['author'] != array()):
                $author = html_entity_decode($data['authors']['author']);
            else :
                $author = 'Redaksi Solopos';
            endif;
            if($xmlObject->posts->properties->category->child != '') {
                $category = $data['properties']['category']['parent'];
                $category_child = $data['properties']['category']['child'];
            } else {
                $category = $data['properties']['category']['parent'];
                $category_child = '';
            }

            // if($subdomain !== $category) {
            //     abort(404);
            // }

            $content = [
                'id' => $data['properties']['post_id'],
                'date' => $data['created'],
                'title' => $data['content']['title'],
                'summary' => $data['content']['summary'] ?? '',
                'content' => $data['content']['content'],
                'slug' => $data['content']['slug'],
                'image' => $image,
                'caption' => $data['images']['caption'] ?? 'Solopos Digital Media - Panduan Informasi dan Inspirasi.',
                'category' => $category,//$data['properties']['category']['parent'],
                'category_parent' => $data['properties']['category']['parent'],
                'category_child' => $category_child,
                'tag' => $data['tags']['tag'],
                'author' => $author,
                'editor' => $data['authors']['editor'],
                'avatar' => $avatar,
                'editor_url' => $data['authors']['editor_url'] ?? '',
                'source' => $data['properties']['post_source'],
            ];
            if($content['category_parent'] == 'cekfakta') {
             $breaking_id = Helper::read_xml(Config::get('xmldata.breaking'), 'breaking-all');
            }else{
             $breaking_id = Helper::read_xml(Config::get('xmldata.breaking'), 'breaking-'.$content['category_parent']);
            }
        } //end conditional post from
        //dd($content['source']);
        $file = getimagesize($content['image']);
        $width = $file[0];
        $height = $file[1];
        // if($relatedTags == ''):
        //     $relatedtag = 72325;
        //     $relatedtitle = 'Info Menarik Untuk Anda';
        // else:
        //     $relatedtag = $relatedTags;
        //     //$relatedtitle = 'Berita Terkait';
        // endif;
        //dd($relatedname);
        $regional = array('banyumas', 'blora', 'grobogan', 'magelang', 'kudus', 'pati', 'pemalang', 'salatiga', 'semarang');
        $if_regional = in_array($content['category_child'], $regional);
        $regional_name = $content['category_child'];
        //dd($content);

        if( is_array($content['tag'])) :
            $keyword = implode(', ', $content['tag']) ?? '';
            $news_keyword = implode(', ', $content['tag']) ?? '';
            $arrayTag = $content['tag'];
        else :
            $keyword = $content['tag'];
            $news_keyword = $content['tag'];
            $arrayTag[] = $content['tag'];
        endif;

        if(!empty($content['summary']) OR $content['summary'] !=array()):
            $summary = html_entity_decode($content['summary']);
            $description = html_entity_decode($content['summary']);
        else :
            $summary = '';
            $description = html_entity_decode($content['title']);
        endif;
        if(!empty($content['summary']) OR $content['summary'] !=array() ):
            $description = html_entity_decode($content['summary']);
        else :
            $description = html_entity_decode($content['title']);
        endif;
        //dd($summary);
        if($content['author'] != array()):
            $author = html_entity_decode($content['author']);
        else :
            $author = 'Redaksi Solopos';
        endif;
        if($content['source'] != array()):
            $source = html_entity_decode($content['source']);
        else:
            $source = '';
        endif;
        $title = html_entity_decode($content['title']);

        if($premium_content == 'premium'):
            $premium = ' Premium';
        else:
            $premium = '';
        endif;

        $header = array(
            'is_single' => 'yes',
            'id' => $content['id'],
            'slug' => $content['slug'],
            'title' => stripslashes($title),
            'description' => stripslashes($description),
            'link'  => 'https://www.solopos.com/'.$content['slug'].'-'.$content['id'],
            'author' => stripslashes($author),
            'editor' => $content['editor'],
            'editor_url' => $content['editor_url'],
            'ringkasan' => stripslashes($summary),
            'keyword' => ucwords($keyword),
            'news_keyword' => ucwords($news_keyword),
            'arrayTag' => $arrayTag,
            'focusKeyword' => ucwords($content['tag'][0] ?? ''),
            'publish_time' => date("Y-m-dTH:i:s+00:00", strtotime($content['date'])),
            'image' => $content['image'],
            'category' => ucwords($content['category']),
            'is_premium' => $premium,
            'category_parent' => ucwords($content['category_parent']),
            'category_child' => ucwords($content['category_child'] ?? ''),
            'img_width' => $width,
            'img_height' => $height,
            'source' => $source
        );
        
        // Breaking after reading
        $category = $content['category_parent'];

        $story = Helper::read_xml(Config::get('xmldata.breaking'), 'breaking-story');
        $news = Helper::read_xml(Config::get('xmldata.breaking'), 'breaking-news');
        $lifestyle = Helper::read_xml(Config::get('xmldata.breaking'), 'breaking-lifestyle');
        $kolom = Helper::read_xml(Config::get('xmldata.breaking'), 'breaking-kolom');
        //$breakingcat = Helper::read_xml(Config::get('xmldata.breaking'), 'breaking-'.$content['category']);
        $jateng = Helper::read_xml(Config::get('xmldata.breaking'), 'breaking-jateng');
        $wisata = Helper::read_xml(Config::get('xmldata.topic'), 'wisata-joglosemar');
        $lifestyle = Helper::read_xml(Config::get('xmldata.breaking'), 'breaking-lifestyle');
        $premium = Helper::read_xml(Config::get('xmldata.breaking'), 'breaking-premium');
        $wisata = Helper::read_xml(Config::get('xmldata.topic'), 'wisata-joglosemar');
		$uksw = Helper::read_xml(Config::get('xmldata.topic'), 'uksw');
        // $popular = Helper::read_xml(Config::get('xmldata.breaking'), 'breaking-popular'); //Http::get('https://tf.solopos.com/api/v1/stats/popular/'.$content['category'])->json();
        $editorchoice = Helper::read_xml(Config::get('xmldata.breaking'), 'breaking-editor-choice');
        // $related = Http::get('https://api.solopos.com/api/wp/v2/posts?tags='.$relatedtag.'&per_page=6')->json();
        $breakingcat = $breaking_id;
        //$widget = Helper::read_xml(Config::get('xmldata.topic'), 'Ekspedisi-Ekonomi-Digital-2021');
        $datawidget = Http::get('https://api.solopos.com/api/breaking/tag/posts?tags=781384');
        $widget = $datawidget->json();
        $dataStories = Http::get('https://api.solopos.com/api/breaking/tag/posts?tags=782886');
        $widgetStories = $dataStories->json();

        $beritaartikel = Http::get('https://api.solopos.com/api/breaking/posts?category=750871');
        $beritafoto = Http::get('https://api.solopos.com/api/breaking/posts?category=750873');
        $beritagrafis = Http::get('https://api.solopos.com/api/breaking/posts?category=750875');
        $artikel = $beritaartikel->json();
        $foto = $beritafoto->json();
        $grafis = $beritagrafis->json();

        // konten promosi (iklan)
        $promote = array_rand($premium);

        if(end($postid) == '1425768') {
            $promosi = $premium[$promote];
        } else {
        // konten promosi (iklan) static
            $promosi = [
                'id' => '1425768',
                 'title' => 'Nimo Highland, Wisata Hits di Bandung yang Mirip Santorini Yunani',
                 'slug' => 'nimo-highland-wisata-hits-di-bandung-yang-mirip-santorini-yunani'
             ];
         }

        $author_data = strtolower($header['author']);
        $author_list = explode('/', $author_data);
        $author_slug = str_replace(' ', '_', $author_list);
        // dd($author_slug[0]);

        if($subdomain !== 'www' AND $subdomain !== $content['category_parent']) {
            abort(404);
        }

        if(!empty($subdomain) AND $subdomain !== 'www') {
            $view = 'regional.read';
        } else {
            $view = 'pages.read';
        }

        //dd($related);

        $bob = array('Foto Wisata Joglosemar', 'Infografis Wisata Joglosemar', 'Wisata Joglosemar','Badan Otorita Borobudur');
        $tematik = array_intersect($bob, $arrayTag);
        $is_bob = '';

		$ukswTag = array('UKSW', 'Prestasi UKSW', 'UKSW Salatiga', 'Foto UKSW Salatiga', 'Prestasi UKSW');
        $tematikuksw = array_intersect($ukswTag, $arrayTag);
        $is_uksw = '';

        $is_bugar = '';

        if($tematik != array()):
            $is_bob = 'yes';
            $breakingcat = $wisata;
        endif;

		if($tematikuksw != array()):
            $is_uksw = 'yes';
            $breakingcat = $uksw;
            $view = 'pages.read-uksw';
        endif;

        if($premium_content == 'premium'):
            $view = 'pages.read-premium';
            $breakingcat = $premium;
        endif;

        if($content['category_parent'] == 'rsjihsolo'):
            $is_bugar = 'yes';
            $view = 'pages.read-bugar';
        endif;
        if($content['category_parent'] == 'bugar'):
            $is_bugar = 'yes';
            $view = 'pages.read-bugar';
        endif;
        if($content['category_parent'] == 'foto'):
            $view = 'pages.read-foto';
        endif;

        if(!empty($amp)) {
            $view = 'pages.amp-read';
        }

        if(!empty($amp) && $premium_content == 'premium') {
            $view = 'pages.amp-read-premium';
        }

        return view($view, [
            'data' => $data,
            'content' => $content,
            'header' => $header,
            'artikel' => $artikel,
            'foto' => $foto,
            'grafis' => $grafis,
            'premium' => $premium,
            /*'popular' => $popular,*/
            'news' => $news,
            'kolom' => $kolom,
            'jateng' => $jateng,
            'lifestyle' => $lifestyle,
            'story' => $story,
            'editorchoice' => $editorchoice,
            'video' => $video,
            'wisata' => $wisata,
            'uksw' => $uksw,
            'breakingcat' => $breakingcat,
            'is_bugar' => $is_bugar,
            'is_bob' => $is_bob,
            'is_uksw' => $is_uksw,
            'widget' => $widget,
            'stories' => $widgetStories,
            'if_regional' => $if_regional,
            'regional_name' => $regional_name,
            'premium_content' => $premium_content,
            'relatedTags' => $relatedTags,
            'promosi' => $promosi,
            'author_slug' => $author_slug,
            'category' => $category,
            'subdomain' => ucfirst($subdomain)
        ]); //'related' => $related, 'relatedtitle' => $relatedtitle,
    }
}
